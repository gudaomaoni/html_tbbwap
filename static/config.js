/*
 * CONFIG：公共配置项
 * @require /static/lib/zepto.min.js
 */

CONFIG = {
	
	// 站点首页路径
	SiteURL: "/template/cis_app/tkk/assets/html/",
	
	// 登录页面地址
	loginURL: "/template/cis_app/tkk/assets/html/login.html",
	
	// 静态文件在站点下的路径
	ROOT: "/template/cis_app/tkk/assets",
	
	// 点赞Ajax交互URL
	likeURL: "/template/cis_app/tkk/assets/test/post.php",
	
	// 关注Ajax交互URL
	followURL: "/template/cis_app/tkk/assets/test/post.php",
	
	// 删除糖控控Ajax URL
	deleteURL: "/template/cis_app/tkk/assets/test/post.php",
	
	// 编辑糖控控Ajax URL
  updateURL: "/template/cis_app/tkk/assets/test/post.php",
	
	// 删除评论Ajax URL
	deleteComeURL: "/template/cis_app/tkk/assets/test/post.php",
	
	// 血糖值正常范围
	// 四个点，3个区间
	// 第1个点（最低）为1.1，第4个点（最高）为33.3
	getBdRange: function(id){
		
		// ele参数解释：
		// 1：凌晨，2：空腹，3：早餐后，4：午餐前，5：午餐后，6：晚餐前，7：晚餐后，8：睡前，99：随机
		
		// 默认正常范围：3.9 ~ 8.0
		var bdArr = [1.1, 3.9, 8.0, 33.3];
		
		// 空腹血糖、餐前血糖正常范围：3.9 ~ 6.1
		if( id == 2 || id == 4 || id == 6) bdArr[2] = 6.1;
		
		// 餐后血糖正常范围：3.9 ~ 8.0
		
		// 返回数据
		return bdArr
		
	},
	
	// 取得血糖时间段名称
	getBdName: function(id){
		
		switch (id)
		{
			case 1:
			  return "凌晨";
			  break;
			  
			case 2:
			  return "空腹";
			  break;
			  
			case 3:
			  return "早餐后";
			  break;
			 
			case 4:
			  return "午餐前";
			  break;
			  
			case 5:
			  return "午餐后";
			  break;
			  
			case 6:
			  return "晚餐前";
			  break;
			  
			case 7:
			  return "晚餐后";
			  break;
			
			case 8:
			  return "睡前";
			  break;
			  
			case 99:
			  return "随机";
			  break;
			
			default:
			  console.log("illegal bloodName ID");
			  break;
		}
	},
	
	// 根据forum的id取得其简化名称
	getForumSimpleName: function(fid){
		
		switch (fid)
		{
			case 327:
			  return "问答";
			  break;
			  
			case 329:
			  return "Ⅰ型";
			  break;
			  
			case 330:
			  return "Ⅱ型";
			  break;
			 
			case 328:
			  return "饮食";
			  break;
			  
			case 332:
			  return "泵";
			  break;
			  
			case 331:
			  return "运动";
			  break;
			  
			case 342:
			  return "妊娠";
			  break;
			
			case 339:
			  return "新人";
			  break;
			  
			case 338:
			  return "心情";
			  break;
			  
			case 340:
			  return "资料";
			  break;
			  
			case 281:
			  return "公告";
			  break;
			  
			case 313:
			  return "反馈";
			  break;

			default:
			  return "帖子";
			  break;
		}
	}
}

// 定义全局变量
window.CONFIG = CONFIG;