/* 
   datePicker.js
   description:滑动选取日期（年，月，日,时间）
   vaersion:1.0
   author:ddd
   https://github.com/ddd702/datePicker
   update：2015-5-5(with iscroll4)
       注意：不能兼容iscroll5，必须用4
   @require zepto.min.js
 */
define(['zepto'], function($){

(function($) {
$.fn.datePicker = function(options) {
    return this.each(function(e) {
        //插件默认选项
        var that = $(this),
            docType = $(this).is('input'),
            nowdate = new Date(),
            yearScroll = null,
            monthScroll = null,
            dayScroll = null,
            hourScroll = null,
            minuteScroll = null,
            initY = null,
            initM = null,
            initD = null,
            initH = null,
            initI = null,
            initS = null,
            initVal = null;
    
        /*使用到的全局函数-e*/
        $.fn.datePicker.defaultOptions = {
            beginyear: 2010, //日期--年--份开始
            endyear: 2020, //日期--年--份结束
            monthDay: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], //日期--12个月天数(默认2月是28,闰年为29)--份结束
            days: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
            beginhour: 0,
            endhour: 23,
            beginminute: 0,
            endminute: 59,
            curdate: false, //打开日期是否定位到当前日期
            liH: 35,
            theme: "date", //控件样式（1：日期(date)，2：日期+时间(datetime),3:时间(time),4:年月(month)）
            mode: null, //操作模式（滑动模式）
            event: "tap", //打开日期插件默认方式为点击后后弹出日期 
            show: true,
            scrollOpt: {
                snap: "li",
                checkDOMChanges: false,
                vScrollbar:false,
                hScrollbar:false,
                hScroll:false
            },
            callBack: function() {}
        }
        var opts = $.extend(true, {}, $.fn.datePicker.defaultOptions, options);
        if(!opts.maxDate)
        {
        	opts.maxDate = new Date()
        }
        
        if (!opts.show) {
            init();
        } else {
            //绑定事件（默认事件为获取焦点）
            that.on(opts.event, function() {
                init();
            });
        }

        function init() { //初始化函数
            if(docType){
                initVal = that.val();
            }
            else initVal = that.html();
            if(!initVal || initVal == '')
            {
            	var now = new Date();
            	initVal=now.getFullYear() + "年" + (now.getMonth()+1)+ "月" + now.getDate() +"日";
            }
            if (!$('#datePlugin').size()) {
                $('body').append('<div id="datePlugin"></div>');
            }
            document.getElementsByTagName('body')[0].addEventListener('touchmove', cancleDefault, false);
            if (!opts.curdate && $.trim(initVal) != '') {
                var inputDate = null,
                    inputTime = null;
                if (opts.theme == 'date' || opts.theme == 'datetime') {
                    inputDate = initVal.split(' ')[0];
                    initY = parseInt(inputDate.split('年')[0] - parseInt(opts.beginyear)) + 1,
                    initM = parseInt(inputDate.split('年')[1].split('月')[0]),
                    initD = parseInt(inputDate.split('年')[1].split('月')[1].split('日')[0]);
                }
                if (opts.theme == 'datetwo') {
                    inputDate = initVal.split(' ')[0];
                    initY = parseInt(inputDate.split('-')[0] - parseInt(opts.beginyear)) + 1,
                    initM = parseInt(inputDate.split('-')[1]),
                    initD = parseInt(inputDate.split('-')[2]);
                }
                if (opts.theme == 'datetime') {
                    inputTime = initVal.split(' ')[1];
                    initH = parseInt(parseFloat(inputTime.split('时')[0]) + 1),
                    initI = parseInt(parseFloat(inputTime.split('时')[1].split('分')[0]) + 1);
                }
                if (opts.theme == 'time') {
                    inputTime = initVal;
                    initH = parseInt(parseFloat(inputTime.split('时')[0]) + 1),
                    initI = parseInt(parseFloat(inputTime.split('时')[1].split('分')[0]) + 1);
                }
                if (opts.theme == 'month') {
                    inputDate = initVal;
                    initY = parseInt(inputDate.split('年')[0] - parseInt(opts.beginyear)) + 1;
                    initM = parseInt(inputDate.split('年')[1].split('月')[0]);
                }
            } else {
            	initY = parseInt(inputDate.split('年')[0] - parseInt(opts.beginyear)) + 1,
                initM = parseInt(inputDate.split('年')[1].split('月')[0]),
                initD = parseInt(inputDate.split('年')[1].split('月')[1].split('日')[0]);
            	initH = parseInt(parseFloat(inputTime.split('时')[0]) + 1),
                initI = parseInt(parseFloat(inputTime.split('时')[1].split('分')[0]) + 1);
                initS = parseInt(nowdate.getSeconds());
            }
            $('#datePlugin').show();
            renderDom();
            $('#d-okBtn').on('click', function(event) {
                document.getElementsByTagName('body')[0].removeEventListener('touchmove', cancleDefault, false);
                var getY = $('#yearScroll li').eq(initY).data('num');
                var getM = $('#monthScroll li').eq(initM).data('num');
                var getD = $('#dayScroll li').eq(initD).data('num');
                that.val($('.d-return-info').html());
                $('#datePlugin').hide().html('');
                // $('#datePlugin').remove();
                var date = new Date(getY,getM-1,getD);
                opts.callBack(date);
                cancleDefault(event);
            });
            $('#d-cancleBtn').on('click', function(event) {
                cancleDefault(event);
                $('#datePlugin').hide().html('');
                // $('#datePlugin').remove();
                document.getElementsByTagName('body')[0].removeEventListener('touchmove', cancleDefault, false);
            });
        }

        function cancleDefault(event) {
            event.preventDefault();
        }

        function isLeap(y) {
            if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
                return true;
            } else {
                return false;
            }
        }

        function renderDom() {
            var mainHtml = ' <div class="d-date-box"><div class="d-date-title">请选择时间</div><p class="d-date-info"><span class="d-day-info"></span><span class="d-return-info"></span></p></div>';
            var btnHtml = '<div class="d-date-btns"><button class="d-btn" id="d-cancleBtn">取消</button><button class="d-btn" id="d-okBtn">确定</button></div>';
            var dateHtml = '<div class="d-date-wrap">';
            dateHtml += '<div class="d-date-mark"></div>';
            dateHtml += '<div class="d-year-wrap d-date-cell" id="yearScroll"><ul></ul></div>';
            dateHtml += '<div class="d-month-wrap d-date-cell" id="monthScroll"><ul></ul></div>';
            dateHtml += '<div class="d-day-wrap d-date-cell" id="dayScroll"><ul></ul></div>';
            dateHtml += '</div>';
            var timeHtml = '<div class="d-date-wrap d-time-wrap">';
            timeHtml += '<div class="d-date-mark"></div>';
            timeHtml += '<div class="d-hour-wrap d-date-cell" id="hourScroll"><ul></ul></div>';
            timeHtml += '<div class="d-minute-wrap d-date-cell" id="minuteScroll"><ul></ul></div>';
            timeHtml += '</div>';
            var monthHtml = '<div class="d-date-wrap">';
            monthHtml += '<div class="d-date-mark"></div>';
            monthHtml += '<div class="d-year-wrap d-date-cell" style="width:50%" id="yearScroll"><ul></ul></div>';
            monthHtml += '<div class="d-month-wrap d-date-cell" style="width:50%" id="monthScroll"><ul></ul></div>';
            monthHtml += '</div>';
            $('#datePlugin').html(mainHtml);
            switch (opts.theme) {
                case 'date':
                    $('.d-date-box').append(dateHtml);
                    createYear();
                    createMonth();
                    createDay(opts.monthDay[initM - 1]);
                    break;
                case 'datetime':
                    $('.d-date-box').append(dateHtml);
                    $('.d-date-box').append(timeHtml);
                    createYear();
                    createMonth();
                    createDay(opts.monthDay[initM - 1]);
                    createHour();
                    createMinute();
                    break;
                case 'time':
                   
                    $('.d-date-box').append(timeHtml);
                    createHour();
                    createMinute();
                    getHourMinute();
                    break;
                case 'month':
                    $('.d-date-box').append(monthHtml);
                    createYear();
                    createMonth();
                    break;
                default:
                    $('.d-date-box').append(dateHtml);
                    createYear();
                    createMonth();
                    createDay(opts.monthDay[initM - 1]);
                    break;
            }
            if(opts.show)  {
                $('.d-date-box').append(btnHtml);

            }else {
                $(".d-date-info").hide();
                $(".d-date-title").hide();
               
            }
            showTxt();
            
        }
        function getHourMinute (){
            if(!opts.show){
                var h = $('#hourScroll li').eq(initH).data('num');
                var i = $('#minuteScroll li').eq(initI).data('num');
                opts.callBack(h,i);
            }
            
        }

        function showTxt() {
            var y = $('#yearScroll li').eq(initY).data('num'),
                m = $('#monthScroll li').eq(initM).data('num'),
                d = $('#dayScroll li').eq(initD).data('num'),
                h = $('#hourScroll li').eq(initH).data('num'),
                i = $('#minuteScroll li').eq(initI).data('num'),
                date = new Date(y + '-' + m + '-' + d);
                 $('.d-return-info').html("");
            switch (opts.theme) {
                case 'date':
                    $('.d-day-info').html(opts.days[date.getDay()] + "&nbsp;");
                    $('.d-return-info').html(y + '年' + m + '月' + d +'日');
                    break;
                    case 'datetwo':
                    $('.d-day-info').html(opts.days[date.getDay()] + "&nbsp;");
                    $('.d-return-info').html(y + '年' + m + '月' + d +'日');
                    break;
                case 'datetime':
                    $('.d-day-info').html(opts.days[date.getDay()] + "&nbsp;");
                    $('.d-return-info').html(y + '年' + m + '月' + d +'日' + ' ' + h + '时' + i +'分');
                    break;
                case 'time':
                    $('.d-return-info').html(h + '时' + i +'分');
                    break;
                case 'month':
                    $('.d-return-info').html(y + '年' + m + '月');
                    break;
                default:
                    $('.d-day-info').html(opts.days[date.getDay()] + "&nbsp;");
                    $('.d-return-info').html(y + '年' + m + '月' + d +'日');
                    break;
            }
        }

        function createYear() {
            var yearDom = $('#yearScroll'),
                yearNum = opts.maxDate.getFullYear() - opts.beginyear,
                yearHtml = '<li></li>';
            for (var i = 0; i <= yearNum; i++) {
                yearHtml += '<li data-num=' + (opts.beginyear + i) + '>' + (opts.beginyear + i) + '年</li>';
            };
            yearDom.find('ul').html(yearHtml).append('<li></li>');
            yearScroll = new iScroll('yearScroll', $.extend(true, {}, opts.scrollOpt, {
                onScrollEnd: function() {
                	 var yIndex = Math.floor(-this.y/opts.liH);
                     initY = yIndex + 1;
                     if (isLeap(parseInt(yearDom.find('li').eq(initY).data('num')))) {
                         opts.monthDay[1] = 29;
                     } else {
                         opts.monthDay[1] = 28;
                     }
                     if (initM == 2 && opts.theme != 'month') {
                         createDay(opts.monthDay[initM - 1]);
                     }
                     showTxt();
                     createMonth();
                     createDay();
                }
            }));
            yearScroll.scrollTo(0, -(initY - 1) * opts.liH);
        }

        function createMonth() {
            var monthDom = $('#monthScroll'),
                monthHtml = '<li></li>';
            //处于时间最大值
            var num = 12;
            if($('#yearScroll li').eq(initY).data('num') == opts.maxDate.getFullYear())
            {
            	num = opts.maxDate.getMonth() + 1;
           	}
            for (var i = 1; i <= num; i++) {
                if (i < 10) {
                    monthHtml += '<li data-num="0' + i + '">0' + i + '月</li>';
                } else {
                    monthHtml += '<li data-num="' + i + '">' + i + '月</li>';
                }
            };
            monthDom.find('ul').html(monthHtml).append('<li></li>');
            monthScroll = new iScroll('monthScroll', $.extend(true, {}, opts.scrollOpt, {
                onScrollEnd: function() {
                	  var mIndex = Math.floor(-this.y/opts.liH);;
                      initM = mIndex + 1;
                      if (opts.theme != 'month') {
                          createDay();
                      }
                      showTxt();
                }
            }));
            if(initM > num)
            {
            	initM = num;
            }
            monthScroll.scrollTo(0, -(initM - 1) * opts.liH);
        }

        function createDay() {
        	var year = $('#yearScroll li').eq(initY).data('num');
        	var month = $('#monthScroll li').eq(initM).data('num');
        	var dayNum = opts.monthDay[month - 1];
            
        	var dayDom = $('#dayScroll'),
                dayHtml = '<li></li>';
            
            //年月处于时间最大值
            if( year == opts.maxDate.getFullYear() &&
            		month == (opts.maxDate.getMonth() + 1))
            {
            	dayNum = opts.maxDate.getDate();
            }
            for (var i = 1; i <= dayNum; i++) {
                if (i < 10) {
                    dayHtml += '<li data-num="0' + i + '">0' + i + '日</li>';
                } else {
                    dayHtml += '<li data-num="' + i + '">' + i + '日</li>';
                }
            };
            dayDom.find('ul').html(dayHtml).append('<li></li>');
            if (dayScroll) {
                dayScroll.destroy();
                dayScroll = null;
            }
            dayScroll = new iScroll('dayScroll',$.extend(true, {}, opts.scrollOpt, {
                onScrollEnd: function() {
                	   initD = Math.floor(-this.y/opts.liH) + 1;
                       showTxt();
                }
            }));
            if (initD > dayNum) {
                initD = dayNum;
            }
            dayScroll.scrollTo(0, -(initD - 1) * opts.liH);
        }

        function createHour() {
            var hourDom = $('#hourScroll'),
                hourHtml = '<li></li>';
            for (var i = opts.beginhour; i <= opts.endhour; i++) {
                if (i < 10) {
                    hourHtml += '<li data-num="0' + i + '">0' + i + '时</li>';
                } else {
                    hourHtml += '<li data-num="' + i + '">' + i + '时</li>';
                }
            };
            hourDom.find('ul').html(hourHtml).append('<li></li>');
            if (hourScroll) {
                hourScroll.destroy();
                hourScroll = null;
            }
            hourScroll = new iScroll('hourScroll', $.extend(true, {}, opts.scrollOpt, {
                onScrollEnd: function() {
                	   initH = Math.floor(-this.y/opts.liH)+ 1;
                       showTxt();
                       getHourMinute();
                }
            }));
            hourScroll.scrollTo(0, -(initH - 1) * opts.liH);
        }

        function createMinute() {
            var minuteDom = $('#minuteScroll'),
                minuteHtml = '<li></li>';
            for (var i = opts.beginminute; i <= opts.endminute; i++) {
                if (i < 10) {
                    minuteHtml += '<li data-num="0' + i + '">0' + i + '分</li>';
                } else {
                    minuteHtml += '<li data-num="' + i + '">' + i + '分</li>';
                }
            };
            minuteDom.find('ul').html(minuteHtml).append('<li></li>');
            if (minuteScroll) {
                minuteScroll.destroy();
                minuteScroll = null;
            }
            minuteScroll = new iScroll('minuteScroll',$.extend(true, {}, opts.scrollOpt, {
                    onScrollEnd: function() {
                    	initI = Math.floor(-this.y/opts.liH) + 1;
                        showTxt();
                        getHourMinute();
                    }
                }));
                minuteScroll.scrollTo(0, -(initI - 1) * opts.liH);
            }
        });
    }
})(Zepto);
	
});

