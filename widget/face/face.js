/* 
 * face：QQ表情模块
 * @require face.css
 */

define(['zepto', 'swiper'], function(require, exports, module){

	var face = function(){
		
		this.target     = null;   // 目标输入框
		this.wrapper    = null;   // HTML容器
		this.controller = null;   // 展示控制按钮
		this.initShow   = false;  // 默认显示/隐藏，需要有控制按钮才能生效
		
		this.status = 'show';    // 标识HTML容器的当前状态[显示/隐藏  show/hide]
		this.moving = false;     // 标识当前正在切换状态中
		
		this.facer  = null;  // 指向生成的表情HTML
		this.swiper = null;  // 指向生成的swiper对象
		this.cursor = {      // 输入框光标位置
			start: 0,
			end: 0
		};
		
		
		/*** 初始化 ****/
		
		this.init = function(options){
			
			this.options = options;
			
			this.target = $("#"+options.tid);
			this.wrapper = $("#"+options.wid);
			
			this.makeHTML();
			this.buildSwiper();
			
			if( options.cid ) this.commander(options);
			this.bindEvent();
			
			// 应用fastclick，加快表情的点击速度
			var FastClick = require('fastclick');
			FastClick.attach(this.wrapper.get(0));
			
		};
		
		
		
		/*** 生成HTML代码 ****/
		
		this.makeHTML = function(){
			
			var F = $('<div class="ui-face"><ul></ul></div>');
			
			for(var i=1; i<=6; i++){
				
				var F_li = $('<li class="ui-face-panel ui-face-panel-'+i+'"></li>');
				
				for( var j=0; j<20; j++){
					
					var n = 20 * (i-1) + j;
					
					F_li.append('<span class="add" index="' + n + '" alt="[em_' + n + ']"></span>');
				}
				
				F_li.append('<span class="delete" index="-1" alt=""></span></li>')
				
				F.find('ul').append( F_li );
			}
			
			this.facer = F;
			
			this.wrapper.append(F);
			
		};
		
		
		
		/**** 应用swiper分屏滑动 ****/
		
		this.buildSwiper = function(){
			
			this.facer.addClass('swiper-container');
			this.facer.find('ul').addClass('swiper-wrapper');
			this.facer.find('li').addClass('swiper-slide');
			this.facer.append('<div class="swiper-pagination"></div>');
			
			this.swiper = new Swiper(this.facer, {
				pagination : '.swiper-pagination',
				paginationClickable :true
			});
			
		};
		
		
		
		/**** 处理控制按钮 ****/
		
		this.commander = function(options){
			
			var that = this,
				cid = options.cid,
				initShow = options.initShow,
				effect = options.effect;
			
			this.controller = $("#"+cid);
			
			// 初始化状态：隐藏
			if( initShow == 'hide' ){
				
				// 如果是动画效果，添加隐藏时所需的样式
				if( effect ) this.wrapper.addClass('ui-face-'+effect);
				else this.wrapper.hide();
				
				this.status = 'hide';
			
			// 初始化状态：显示
			}else if( initShow == 'show' ){
				
				// 如果是动画效果，应用显示动画
				if( effect ) this.wrapper.addClass('ui-face-'+effect+'-in');
				this.controller.addClass('on');
				
				this.status = 'show';
			}
			
			// 控制按钮点击事件
			this.controller.on('click', function(){
				
				// 如果当前正在动画中，取消动作
				if( that.moving ) return false;
				
				if( effect ) that.doSomeEffect(effect);
				else that.wrapper.toggle();
				
				$(this).toggleClass('on');
			});
		};
		
		
		
		/**** 应用动画效果 ****/
		
		this.doSomeEffect = function(effect){
			
			var that = this;
			
			// 根据status参数来进行动画的执行
			// 注意：动画为CSS3效果，如需添加动画效果，CSS样式的命令要遵循 .ui-face-[effect]-in 和  .ui-face-[effect]-out 这样的规则
			
			if( this.status == 'show'){
				
				this.moving = true;
				
				this.wrapper.removeClass('ui-face-'+effect+'-in').addClass('ui-face-'+effect+'-out');
				
				// 改变状态标识
				setTimeout(function(){ that.status = 'hide'; that.moving = false; }, 300);
			
			}else{
				
				this.moving = true;
				
				this.wrapper.removeClass('ui-face-'+effect+'-out').addClass('ui-face-'+effect+'-in');
				
				// 改变状态标识
				setTimeout(function(){ that.status = 'show'; that.moving = false; }, 400);
			}	
			
		};
		
		
		
		/**** 绑定事件 ****/
		
		this.bindEvent = function(){
			
			var that = this;
			
			// 记录光标在输入框中的位置
			this.target.on('blur', function() {
				
		        that.cursor.start = this.selectionStart;
		        that.cursor.end = this.selectionEnd;
		        
		    });
					
			// 表情点击
			this.facer.find('.add').on('click', function(){
				
				var text = that.target.val(),
					code = $(this).attr('alt');
				
	            // 添加表情code块到光标位置
	            var resText = text.substr(0, that.cursor.start) + code + text.substr(that.cursor.end, text.length);
	            that.target.val(resText);
	            
	            // 触发目标输入框的input事件
	            that.target.trigger('input');
	            
	            // 调整光标位置：最后面
	            that.cursor.start = that.cursor.end = that.cursor.end + code.length;
	            
			});
			
			// 删除按钮点击
			this.facer.find('.delete').on('click', function(){
				
                var text = that.target.val();
                
                // 获取光标之前的字符串
                var changedText = text.substr(0, that.cursor.start);
                var len = changedText.length;
                var reg = /\[em_([0-9]*)\]$/g;
                
                // 删除表情code块或最后一个字符
                if (reg.test(changedText)) {
                    changedText = changedText.replace(reg, "");
                } else {
                    changedText = changedText.substring(0, changedText.length - 1);
                }
                
                var resText = changedText + text.substr(that.cursor.end, text.length);
                
                that.target.val(resText);
                
                // 触发目标输入框的input事件
	            that.target.trigger('input');
                
                // 调整光标位置
                that.cursor.start = that.cursor.end = that.cursor.end - (len - changedText.length);
				
			});
		};
		
		
		
		/**** 隐藏表情接口 ****/
		
		// 此接口暴露给外部环境使用
		
		this.back = function(){
			
			// 表情区域已经处于隐藏状态，无需再操作
			if( this.status == 'hide' ) return false;
			
			if( this.options.effect ) this.doSomeEffect(this.options.effect);
			else this.wrapper.toggle();
			
			if( this.controller ) this.controller.removeClass('on');
		};
		
	}
	
	return face

});