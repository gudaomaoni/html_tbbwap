/*
 * FUNC：公共函数集
 * 站点公共函数集合
 * @require /static/lib/zepto.min.js
 * @require /widget/base/config.js
 */

FUNC = {

  // 糖控控区域：点赞 、取消点赞
  liked: function(ele, moodId) {

    // 判断登录和关注
    if(!checkLogin()) return;

    var $ele = $(ele),
      icon = $ele.find('i'),
      count = $ele.find('span'),
      countValue = parseInt(count.html());

    // 取消点赞
    if(icon.hasClass('on')) {

      $.post(CONFIG.likeURL, {
        action: 'unlike',
        id: moodId,
        type: 'tkk'
      }, function(response) {

        var data = eval("(" + response + ")");

        if(data.success == true) {

          icon.removeClass('on');
          count.html(countValue - 1);

        } else {

          alert('出错啦！请与工作人员进行联系~');
        }

      });

      // 点赞	
    } else {

      $.post(CONFIG.likeURL, {
        action: 'like',
        id: moodId,
        type: 'tkk'
      }, function(response) {

        var data = eval("(" + response + ")");

        if(data.success == true) {

          icon.addClass('on');
          count.html(countValue + 1);

        } else {

          alert('出错啦！请与工作人员进行联系~');
        }

      });

    }

  },

  // 糖控控区域：关注、取消关注
  followd: function(ele, userID) {

    // 判断登录和关注
    if(!checkLogin()) return;

    var $ele = $(ele);

    // 取消关注
    if($ele.hasClass('on')) {

      $.ajax({

        type: "POST",
        url: CONFIG.followURL,
        data: {
          op: 'del',
          fuid: userID,
          hash: tbb.formhash
        },
        dataType: "xml"

      }).success(function(response) {

        if(response.lastChild.firstChild.nodeValue == 'ok') {

          $ele.removeClass('on').html("关注");

          // 首页此用户其他说说也显示 +关注
          $(".indexMoodItem").filter("[data-userid=" + userID + "]").find('.indexMoodFollow').removeClass('on').html("关注");

        } else {

          alert(response);
        }

      });

      // 关注
    } else {

      $ele.addClass('load').html("加载中");

      $.ajax({

        type: "POST",
        url: CONFIG.followURL,
        data: {
          op: 'add',
          fuid: userID,
          hash: tbb.formhash
        },
        dataType: "xml"

      }).success(function(response) {

        if(response.lastChild.firstChild.nodeValue == 'ok') {

          $ele.removeClass('load').addClass('on').html("已关注");

          // 首页此用户其他说说也显示已关注
          $(".indexMoodItem").filter("[data-userid=" + userID + "]").find('.indexMoodFollow').addClass('on').html("已关注");

        } else {

          alert(response);
        }

      });

    }

  },

  // 取消关注
  unfollow: function(userID) {

    // 判断登录和关注
    if(!checkLogin()) return;

    layer.open({
      type: 2
    });

    $.ajax({

      type: "POST",
      url: CONFIG.followURL,
      data: {
        op: 'del',
        fuid: userID,
        hash: tbb.formhash
      },
      dataType: "xml"

    }).success(function(response) {

      layer.closeAll();

      if(response.lastChild.firstChild.nodeValue == 'ok') {

        layer.open({
          content: '已取消关注 √',
          btn: ['好的']
        });

      } else {

        alert(response);
      }

    });
  },

  // 删除糖控控
  deleteTkk: function(id) {

    // 判断登录和关注
    if(!checkLogin()) return;

    layer.open({
      type: 2
    });

    $.post(CONFIG.deleteURL, {
      action: 'delete',
      id: id,
      hash: tbb.formhash
    }, function(response) {

      var data = eval("(" + response + ")");

      layer.closeAll();

      if(data.success == true) {

        layer.open({
          content: '已删除说说 √',
          btn: ['好的']
        });

        if(typeof(FUNC.popDataEle) != 'undefined') $(FUNC.popDataEle).parent().remove();

      } else {

        alert('出错啦！请与工作人员进行联系~');
      }

    });
  },

  // 弹出底部提示框：第一层
  showBotPop: function(cont, id, ele) {

    // 判断登录和关注
    if(!checkLogin()) return false;

    var pop = $("#" + cont);

    pop.removeClass('ui-popup-hide').addClass('ui-popup-show').css('display', 'block');

    $('#ui-popup-dark').css('display', 'block').off()
      .on('click', function() {
        FUNC.hideBotPop(cont);
      });

    if(cont == "actionSystem" || cont == "actionEdit" || cont == "actionCome") {
      FUNC.popDataId = id;
      FUNC.popDataEle = ele;
    }
  },

  // 弹出底部提示框：第二层
  showBotPop2: function(source, target) {

    $("#" + source).removeClass('ui-popup-show').addClass('ui-popup-hide');
    FUNC.showBotPop(target);
  },

  // 关闭底部提示框
  hideBotPop: function(cont) {

    $("#" + cont).removeClass('ui-popup-show').addClass('ui-popup-hide');
    $('#ui-popup-dark').css('display', 'none');
  },

  // 确定删除糖控控
  confirmDelTkk: function() {

    FUNC.hideBotPop('actionSystem_confirm');
    FUNC.deleteTkk(FUNC.popDataId);
  },

  // 确定不再关注
  confirmUnfollow: function() {

    FUNC.hideBotPop('actionEdit_confirm');
    FUNC.unfollow(FUNC.popDataId);
  },

  // 确定删除评论
  confirmDelCome: function() {

    FUNC.hideBotPop('actionCome_confirm');

    layer.open({
      type: 2
    });

    $.post(CONFIG.deleteComeURL, {
      action: 'delete',
      id: FUNC.popDataId,
      hash: tbb.formhash
    }, function(response) {

      var data = eval("(" + response + ")");

      layer.closeAll();

      if(data.success == true) {

        layer.open({
          content: '已删除评论 √',
          btn: ['好的']
        });

        if(typeof(FUNC.popDataEle) != 'undefined') FUNC.popDataEle.remove();

      } else {

        alert('出错啦！请与工作人员进行联系~');
      }

    });

  },
  
  // 取得URL地址栏参数的值
  getQueryString: function(name) {
    
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r != null) return unescape(r[2]);
    return null;
  }
}

// 定义全局变量
window.FUNC = FUNC;