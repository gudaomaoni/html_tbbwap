/*
 * PAGE：Mobilebone页面转场执行的函数集
 * 以下函数默认为每次页面进入时执行的函数，如不是，请进行注释说明
 * @require /static/lib/require.js
 * @require /static/lib/mobilebone.js
 * @require /static/lib/zepto.min.js
 * @require /widget/base/config.js
 * @require /widget/utils/func.js
 */


PAGE = {
	
	// index.html 首页
	// 第一次载入时执行
	indexFirst: function(pageInto, pageOut, options){
		
		// 加载Ajax请求页面所需的CSS样式
		
	},
	
	// index.html 首页
	index: function(pageInto, pageOut, options){
		
	},
	
	// blood_add.html 血糖添加页面
	// 第一次加载的时候执行
	bloodAddFirst: function(pageInto, pageOut, options){
		
		// 将返回箭头的ID修改为发起加载请求的页面
		// var bloodBack = pageInto.querySelector("#bloodAddBack");
		// bloodBack.href = "#" + pageOut.id;
		
	},
	
	// blood_add.html 血糖添加页面
	bloodAdd: function(pageInto, pageOut, options){
		
		// 如果血糖值为0，血糖值输入框获得焦点：设置350ms延迟防止破坏页面切换动画
		// 在微信浏览器上主动获得焦点无效，必须要有动作才能，等待破解方法

	},
	
	// 记录血糖页面：数据记录，返回true则阻断页面跳转
	bloodAddGate: function(pageInto, pageOut, options){
		
		var bloodValue = $("#bloodAdd_value").val();

		if( bloodValue == 0 || bloodValue == ""){
			layer.open({
			    content: '请填写血糖值！',
			    className: 'layerInfo',
			    time: 2,
			    end: function(){
			    	$("#bloodAdd_value").trigger('focus')
			    }
			});
			
			return true;
		}
		
		return false
	},
	
	// 记录血糖页面：数据发布，每次进入时执行函数
	bloodPost: function(pageInto, pageOut, options){
		
		// 将bloodAdd页面的数据获取并展现到页面上
		var bloodValue = parseFloat($("#bloodAdd_value").val()),
			bloodStage = parseInt($("#bloodAdd_stage").val()),
			bloodFood  = $("#bloodAdd_food").val(),
			bloodDrug  = $("#bloodAdd_drug").val(),
			bloodSport = $("#bloodAdd_sport").val(),
			bloodDate  = $("#bloodAdd_date").val();
		
		$("#bloodPostValue").html( bloodValue ? bloodValue : 0 );
		$("#bloodPostStage").html( CONFIG.getBdName(bloodStage) );
		$("#bloodPostFood").html( bloodFood );
		$("#bloodPostDrug").html( bloodDrug );
		$("#bloodPostSport").html( bloodSport );
		$("#bloodPostDate").html( bloodDate );
		
		// 判断更改血糖颜色
		var bdArr = CONFIG.getBdRange(bloodStage),
			$moodBV = $('#bloodPostDataContent .ui-moodBV');
		
		if( bloodValue < bdArr[1] ) $moodBV.addClass('ui-moodBV3');
		else if( bloodValue > bdArr[2] ) $moodBV.addClass('ui-moodBV2');
		
		//console.log('bloodPost comein');
	},
	
	// 是否登录和关注
	checkLogin: function(){
		
		// 判断登录和关注
		if( !checkLogin() ){
			console.log('未登录或未关注')
			return true;
		} 
		else{
			console.log('已登录，已关注')
			return false
		} 
		
		// 返回true阻断页面过场
	}
	
}

// 定义全局变量
window.PAGE = PAGE;
