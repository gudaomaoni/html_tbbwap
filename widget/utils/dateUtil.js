/**
 * 日期处理工具类
 */
var DateUtil = {};

/**
 * 日期对象转换为指定格式的字符串
 * 
 * @param f
 *            日期格式,格式定义如下 yyyy-MM-dd HH:mm:ss
 * @param date
 *            Date日期对象, 如果缺省，则为当前时间
 * 
 * YYYY/yyyy/YY/yy 表示年份 MM/M 月份 W/w 星期 dd/DD/d/D 日期 hh/HH/h/H 时间 mm/m 分钟
 * ss/SS/s/S 秒
 * @return string 指定格式的时间字符串
 */
DateUtil.dateToStr = function(date, formatStr) {
	date = arguments[0] || new Date();
	formatStr = arguments[1] || "yyyy-MM-dd HH:mm:ss";
	var str = formatStr;
	var Week = ['日', '一', '二', '三', '四', '五', '六'];
	str = str.replace(/yyyy|YYYY/, date.getFullYear());
	str = str.replace(/yy|YY/,
		(date.getYear() % 100) > 9 ? (date.getYear() % 100).toString() : '0' + (date.getYear() % 100));
	str = str.replace(/MM/, (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1));
	str = str.replace(/M/g, (date.getMonth() + 1));
	str = str.replace(/w|W/g, Week[date.getDay()]);

	str = str.replace(/dd|DD/, date.getDate() > 9 ? date.getDate().toString() : '0' + date.getDate());
	str = str.replace(/d|D/g, date.getDate());

	str = str.replace(/hh|HH/, date.getHours() > 9 ? date.getHours().toString() : '0' + date.getHours());
	str = str.replace(/h|H/g, date.getHours());
	str = str.replace(/mm/, date.getMinutes() > 9 ? date.getMinutes()
		.toString() : '0' + date.getMinutes());
	str = str.replace(/m/g, date.getMinutes());

	str = str.replace(/ss|SS/, date.getSeconds() > 9 ? date.getSeconds()
		.toString() : '0' + date.getSeconds());
	str = str.replace(/s|S/g, date.getSeconds());

	return str;
};


/*
 * 参数：[year, month] year(年份)  month(月份)
 * 参数类型：array[int,int]
 * 返回：XX年XX月
 * 返回类型：string
 */
DateUtil.YMtoString = function(arr){
	
	var str = "";
	str += arr[0] + "年";
	str += ( (arr[1]+1>9)?arr[1]:'0'+arr[1] ) + "月";
	return str
};

/*
 * 作用：返回上一个月的年份和月份的数组集合
 * 参数：[year, month]
 * 参数类型：array[int,int]
 * 返回：[year, month]
 * 返回类型：array[int,int]
 */
DateUtil.MonthPrevYM = function(arr){
	
	var year = arr[0],
		month = arr[1];
		
	if(month > 1) month = month -1;
	else{
		year = year - 1;
		month = 12
	}
	
	return [year, month]
};

/*
 * 作用：返回下一个月的年份和月份的数组集合
 * 参数：[year, month]
 * 参数类型：array[int,int]
 * 返回：[year, month]
 * 返回类型：array[int,int]
 */
DateUtil.MonthNextYM = function(arr){
	
	var year = arr[0],
		month = arr[1];
		
	if(month < 12) month = month + 1;
	else{
		year = year + 1;
		month = 1
	}
	
	return [year, month]
};


/*
 * 参数：month
 * 参数类型：string or int
 * 返回：XX月
 * 返回类型：string
 */
DateUtil.getMonthString = function(month){
	
	var m = parseInt(month);
	
	switch (m) {
		case 1:
			return "一月";
		case 2:
			return "二月";
		case 3:
			return "三月";
		case 4:
			return "四月";
		case 5:
			return "五月";
		case 6:
			return "六月";
		case 7:
			return "七月";
		case 8:
			return "八月";
		case 9:
			return "九月";
		case 10:
			return "十月";
		case 11:
			return "十一月";
		case 12:
			return "十二月";
	}
};


/**
 * 指定日期单位计算
 * 
 * @param strInterval
 *            string 可选值 y 年 m月 d日 w星期 ww周 h时 n分 s秒
 * @param num
 *            int
 * @param date
 *            Date 日期对象
 * @return Date 返回日期对象
 */
DateUtil.dateAdd = function(strInterval, num, date) {
	date = arguments[2] || new Date();
	switch (strInterval) {
		case 's':
			return new Date(date.getTime() + (1000 * num));
		case 'n':
			return new Date(date.getTime() + (60000 * num));
		case 'h':
			return new Date(date.getTime() + (3600000 * num));
		case 'd':
			return new Date(date.getTime() + (86400000 * num));
		case 'w':
			return new Date(date.getTime() + ((86400000 * 7) * num));
		case 'm':
			return new Date(date.getFullYear(), (date.getMonth()) + num, date
				.getDate(), date.getHours(), date.getMinutes(), date
				.getSeconds());
		case 'y':
			return new Date((date.getFullYear() + num), date.getMonth(), date
				.getDate(), date.getHours(), date.getMinutes(), date
				.getSeconds());
	}
};

/**
 * 比较日期差 dtEnd 格式为日期型或者有效日期格式字符串
 * 
 * @param strInterval
 *            string 可选值 y 年 m月 d日 w星期 ww周 h时 n分 s秒
 * @param dtStart
 *            Date 可选值 y 年 m月 d日 w星期 ww周 h时 n分 s秒
 * @param dtEnd
 *            Date 可选值 y 年 m月 d日 w星期 ww周 h时 n分 s秒
 */
DateUtil.dateDiff = function(strInterval, dtStart, dtEnd) {
	switch (strInterval) {
		case 's':
			return parseInt((dtEnd - dtStart) / 1000);
		case 'n':
			return parseInt((dtEnd - dtStart) / 60000);
		case 'h':
			return parseInt((dtEnd - dtStart) / 3600000);
		case 'd':
			return parseInt((dtEnd - dtStart) / 86400000);
		case 'w':
			return parseInt((dtEnd - dtStart) / (86400000 * 7));
		case 'm':
			return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
		case 'y':
			return dtEnd.getFullYear() - dtStart.getFullYear();
	}
};

/**
 * 判断闰年
 * 
 * @param date
 *            Date日期对象
 * @return boolean true 或false
 */
DateUtil.formatDate = function(year, month, dateo, hour, mm) {

	return new Date(year, month - 1, dateo, hour, mm);
}
DateUtil.isLeapYear = function(date) {
	return (0 == date.getYear() % 4 && ((date.getYear() % 100 != 0) || (date
		.getYear() % 400 == 0)));
};

/**
 * 
 */
DateUtil.formatStrDate = function(dateStr) {
	var formatStr = "yyyy-MM-dd HH:mm:ss";
	var year = 0;
	var start = -1;
	var len = dateStr.length;
	if ((start = formatStr.indexOf('yyyy')) > -1 && start < len) {
		year = dateStr.substr(start, 4);
	}
	var month = 0;
	if ((start = formatStr.indexOf('MM')) > -1 && start < len) {
		month = parseInt(dateStr.substr(start, 2)) - 1;
	}
	var day = 0;
	if ((start = formatStr.indexOf('dd')) > -1 && start < len) {
		day = parseInt(dateStr.substr(start, 2));
	}
	var hour = 0;
	if (((start = formatStr.indexOf('HH')) > -1 || (start = formatStr
			.indexOf('hh')) > 1) && start < len) {
		hour = parseInt(dateStr.substr(start, 2));
	}
	var minute = 0;
	if ((start = formatStr.indexOf('mm')) > -1 && start < len) {
		minute = dateStr.substr(start, 2);
	}
	var second = 0;
	if ((start = formatStr.indexOf('ss')) > -1 && start < len) {
		second = dateStr.substr(start, 2);
	}
	return DateUtil.dateToStr(new Date(year, month, day, hour, minute, second));
};

DateUtil.strToDate = function(dateStr) {
		if (!dateStr) {
			return new Date();
		}
		var formatStr = "yyyy-MM-dd HH:mm:ss";
		var year = 0;
		var start = -1;
		var len = dateStr.length;
		if ((start = formatStr.indexOf('yyyy')) > -1 && start < len) {
			year = dateStr.substr(start, 4);
		}
		var month = 0;
		if ((start = formatStr.indexOf('MM')) > -1 && start < len) {
			month = parseInt(dateStr.substr(start, 2)) - 1;
		}
		var day = 0;
		if ((start = formatStr.indexOf('dd')) > -1 && start < len) {
			day = parseInt(dateStr.substr(start, 2));
		}
		var hour = 0;
		if (((start = formatStr.indexOf('HH')) > -1 || (start = formatStr
				.indexOf('hh')) > 1) && start < len) {
			hour = parseInt(dateStr.substr(start, 2));
		}
		var minute = 0;
		if ((start = formatStr.indexOf('mm')) > -1 && start < len) {
			minute = dateStr.substr(start, 2);
		}
		var second = 0;
		if ((start = formatStr.indexOf('ss')) > -1 && start < len) {
			second = dateStr.substr(start, 2);
		}
		return new Date(year, month, day, hour, minute, second);
	}
	/**
	 * 日期对象转换为毫秒数
	 */
DateUtil.dateToLong = function(date) {
	return date.getTime();
};

/**
 * 毫秒转换为日期对象
 * 
 * @param dateVal
 *            number 日期的毫秒数
 */
DateUtil.longToDate = function(dateVal) {
	return new Date(dateVal);
};


/**
 * 判断字符串是否为日期格式
 * 
 * @param str
 *            string 字符串
 * @param formatStr
 *            string 日期格式， 如下 yyyy-MM-dd
 */
DateUtil.isDate = function(str, formatStr) {
	if (formatStr == null) {
		formatStr = "yyyyMMdd";
	}
	var yIndex = formatStr.indexOf("yyyy");
	if (yIndex == -1) {
		return false;
	}
	var year = str.substring(yIndex, yIndex + 4);
	var mIndex = formatStr.indexOf("MM");
	if (mIndex == -1) {
		return false;
	}
	var month = str.substring(mIndex, mIndex + 2);
	var dIndex = formatStr.indexOf("dd");
	if (dIndex == -1) {
		return false;
	}
	var day = str.substring(dIndex, dIndex + 2);
	if (!isNumber(year) || year > "2100" || year < "1900") {
		return false;
	}
	if (!isNumber(month) || month > "12" || month < "01") {
		return false;
	}
	if (day > getMaxDay(year, month) || day < "01") {
		return false;
	}
	return true;
};

/**
 * 获取月份的最大天数
 */
DateUtil.getMaxDay = function(year, month) {
	if (month == 4 || month == 6 || month == 9 || month == 11)
		return "30";
	if (month == 2)
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
			return "29";
		else
			return "28";
	return "31";
};

/**
 * 变量是否为数字
 */
DateUtil.isNumber = function(str) {
	var regExp = /^\d+$/g;
	return regExp.test(str);
};

/**
 * 把日期分割成数组 [年、月、日、时、分、秒]
 */
DateUtil.toArray = function(myDate) {
	myDate = arguments[0] || new Date();
	var myArray = Array();
	myArray[0] = myDate.getFullYear();
	myArray[1] = myDate.getMonth();
	myArray[2] = myDate.getDate();
	myArray[3] = myDate.getHours();
	myArray[4] = myDate.getMinutes();
	myArray[5] = myDate.getSeconds();
	return myArray;
};

/**
 * 取得日期数据信息 参数 interval 表示数据类型 y 年 M月 d日 w星期 ww周 h时 n分 s秒
 */
DateUtil.datePart = function(interval, myDate) {
	myDate = arguments[1] || new Date();
	var partStr = '';
	var Week = ['日', '一', '二', '三', '四', '五', '六'];
	switch (interval) {
		case 'y':
			partStr = myDate.getFullYear();
			break;
		case 'M':
			partStr = myDate.getMonth() + 1;
			break;
		case 'd':
			partStr = myDate.getDate();
			break;
		case 'w':
			partStr = Week[myDate.getDay()];
			break;
		case 'ww':
			partStr = myDate.WeekNumOfYear();
			break;
		case 'h':
			partStr = myDate.getHours();
			break;
		case 'm':
			partStr = myDate.getMinutes();
			break;
		case 's':
			partStr = myDate.getSeconds();
			break;
	}
	return partStr;
};

/**
 * 计算时间差，并返回对应的字符串表达方式
 */
DateUtil.difftimeToStr = function(timeCurrent, timeCompared) {
	if (!timeCurrent) {
		timeCurrent = new Date().getTime();
	}
	var d_minutes, d_hours, d_days;
	var timeNow = parseInt(timeCurrent / 1000);
	var d;
	d = timeNow - timeCompared; // s
	d_years = d / 31104000;
	d_months = d / 2592000;
	d_days = d / 86400;
	d_hours = d / 3600;
	d_minutes = d / 60;
	d_seconds = d;
	
	//console.log( timeCompared, timeCurrent, d, d_years, d_months, d_days, d_hours, d_minutes, d_seconds)
	
	if ( d_years >= 1 ) {
		return parseInt(d_years) + "年前";
	} else if ( d_months >= 1 ) {
		return parseInt(d_months) + "个月前";
	} else if ( d_days >= 1 ) {
		return parseInt(d_days) + "天前";
	} else if ( d_hours >= 1 ) {
		return parseInt(d_hours) + "小时前";
	} else if ( d_minutes >= 1 ) {
		return parseInt(d_minutes) + "分钟前";
	} else {
		if( d_seconds <= 0) return "刚刚";
		else return d_seconds + "秒前";
	}
}


/**
 * 计算时间差，并返回相差的文字（XX天XX时XX分）
 */
DateUtil.difftimeToStr2 = function(timeCurrent, timeCompared) {
  if (!timeCurrent) {
    timeCurrent = new Date().getTime();
  }
  var d_minutes, d_hours, d_days;
  var d;
  d =  Math.abs(timeCurrent - timeCompared); // s
  //console.log(timeCurrent, timeCompared)
  d_days = Math.floor( d / 86400 );
  // 剩余的秒数
  d = d - d_days*86400;
  d_hours = Math.floor( d / 3600 );
  // 剩余的秒数
  d = d - d_hours*3600;
  d_minutes = Math.floor( d / 60 );
  
  d_seconds = Math.floor(d - d_minutes*60);

  
  return d_days+"天"+d_hours+"时 "+d_minutes+"分"+d_seconds+"秒";
  
  //console.log( timeCompared, timeCurrent, d, d_years, d_months, d_days, d_hours, d_minutes, d_seconds)
  
  
}



/**
 * 取得当前日期所在月的最大天数
 */
DateUtil.maxDayOfDate = function(date) {
	date = arguments[0] || new Date();
	date.setDate(1);
	date.setMonth(date.getMonth() + 1);
	var time = date.getTime() - 24 * 60 * 60 * 1000;
	var newDate = new Date(time);
	return newDate.getDate();
};

/**
 * 将2015年12月12日转成 2015-12-12
 */
DateUtil.datePickerStrToStr = function(dateStr) {
	var year = dateStr.split("年")[0];
	var month = dateStr.split("年")[1].split("月")[0];
	var day = dateStr.split("年")[1].split("月")[1].split("日")[0];
	return year + "-" + month + "-" + day;
}

// 定义全局变量
window.DateUtil = DateUtil;