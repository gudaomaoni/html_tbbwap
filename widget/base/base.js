/* 
 * Base：全局配置
 * 每个页面都需要引用的基础JS代码
 * @require /static/lib/require.js
 */


define(['/widget/utils/func','/widget/utils/page', '/widget/utils/dateUtil','/static/config','zepto'], function (require, exports, module) {
    
	// 解决300ms点击延迟效果
	// 解决点透现象，微信浏览器iscroll里面click触发两次BUG
	// 将iscroll设置为不允许click，引用fastclick监听点击事件
	// var FastClick = require('fastclick');
	// FastClick.attach(document.body);
	// 暂时禁用FastClick，安卓下不兼容
	
	// 如果没有来路页面，点击返回按钮进入首页
	var hisLength = window.history.length;
	
	if( hisLength <= 1 ){
		
		$(".imui_icon_back").each(function(){
			
			if( !$(this).attr('back') ){
				
				$(this).parent().html('<a href="'+ CONFIG.SiteURL +'" class="imui_logo"><img style="height: 30px; margin-top: 10px; margin-left: 8px;" src="'+ __uri('images/logo.png') +'"></a>');
				
			}
			
		});
		
	}
	
});