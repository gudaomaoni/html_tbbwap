/* 
 * graph：曲线图模块
 * 说明：随机血糖不在此曲线图中展示（考虑到空间有限，只展示重要的数据）
 */


define(['/static/lib/d3','zepto', '/static/config'], function(require, exports, module){
	
	var GP = function(){
		
		this.defaultOptions = {
			
			container: "#md-graph",       // 需要插入SVG进行图形展示的元素
			padding: [20, 20, 30, 36],    // 图形的间距
			controller: "#md-control",    // 控制血糖类别的元素
			overturn: true                // 是否支持转屏
		};
		
		this.fullData = null;     // 数据源：初始时拉取所有数据，之后的操作从这里进行数据计算，不再向服务器拉取
		this.baseType = 0;        // 当前展示的数据类型（凌晨、睡前、空腹....），默认为0，即全部数据
		this.baseData = null;     // 当前选择的时间段的数据（根据选择的数据类型从数据源获取）
		
		this.level = 0;                // 当前缩放级别
		this.xAxisDays = [20, 5, 60];      // x轴展示的天数
		this.xAxisUnit = [4, 5, 2];       // x轴刻度数量，这里只是一个大概数值，d3在展示的时候会自动优化
		
		this.showData = null;     // 当前展示所需的数据（根据X轴的区间从baseData中获取）
		this.showStartTime = 0;   // 当前展示图形的X轴的起始时间
		this.showEndTime = 0;     // 当前展示图形的X轴的结束时间
		
		this.init = function(options, data){
			
			// 扩展options
			this.options = $.extend(this.defaultOptions, options);
			this.container = $(this.options.container);
			
			this.svgWidth = this.container.width();
			this.svgHeight = this.container.height();
			
			// 添加CSS样式
			this.container.addClass('md-graph');
			
			// 计算图形区域的实际宽高度（除去坐标轴的区域）
			this.width = this.svgWidth - this.options.padding[3];
			this.height = this.svgHeight - this.options.padding[0] - this.options.padding[2];
			
			// 上面的this.width用来做展示，实际X轴占据的宽度要减去padding right
			this.xWidth = this.width - this.options.padding[1];
			
			// 定义X轴、Y轴的缩放比
			this.x = d3.time.scale().range([0, this.xWidth]);
	    	this.y = d3.scale.linear().range([this.height, 0]).domain([0, 11]);
			
			this.initSVG();
			this.paintY();
			this.initGraph(data);
			
			if( this.options.overturn ) this.applyOverTurn();
		};
		
		
		/*** 生成图形所需要的DOM元素 ***/
		this.initSVG = function(){
			
			// 绘制SVG和G
			this.G = d3.select(this.options.container)
						 .append('svg:svg')
						 .attr({ 'width':this.svgWidth, 'height':this.svgHeight })
						 .append("svg:g")
						 .attr("transform", "translate(" + this.options.padding[3] + "," + this.options.padding[0] + ")");
			
			
			this.G.append("svg:clipPath")
					.attr("id", "clip")
					.append("svg:rect")
					.attr("x", 0)
					.attr("y", 0)
					.attr("width", this.width)
					.attr("height", this.height);
					
			// 插入Y轴血糖正常提示区间
			this.G.append("rect").attr("class", "normal")
					.attr("x", 0)
	                .attr("y", this.glucoseData2Y(8)).attr("width", this.width)
	                .attr("height", this.glucoseData2Y(4.0) - this.glucoseData2Y(8)).style("fill", "#00A0E9");
			    
		};
		
		
		/*** 初始：加载数据、绘制曲线 ***/
		this.initGraph = function(data){
			
			var _this = this, data = data;
			
		    // 对数据进行格式处理
		    data.forEach(function(d) {
		        d.x = +(d.date*1000);                  // X坐标值转换为时间戳，因Unix时间戳单位为秒，这里*1000转换为毫秒
		        d.y = +_this.glucoseData2Y(d.value);   // Y坐标值转换为对应的值
		    });
			    
		    // 将数据集合顺序进行倒置，方便之后对其进行操作
		    var newData = new Array();
	        if (data) {
	            for (var j = data.length - 1; j >= 0; j--) {
	                newData.push(data[j]);
	            }
	        }
		    
		    // 存储数据
		    this.fullData = newData;
		    this.baseData = newData;
			
			this.initShowTime();
			
			this.applyZoom();
			
			// 绘制曲线
		    this.renderGraph(newData);

            this.applyChangeType();
		};
			
		
		/*** 初始载入时，计算时间跨度 ***/
		this.initShowTime = function(){
			
			var startTime, endTime, nowTime = new Date();
			
			// 结束时间定为当前现实中的时间
			endTime = nowTime.getTime();
			
			// 86400000为1天的毫秒数
			startTime = endTime - ( this.xAxisDays[this.level] * 86400000 );
			
			this.showStartTime = startTime;
			this.showEndTime = endTime;
			
			console.log("展示数据时间跨度：" + new Date(startTime), new Date(endTime) )
			
		};
		
		
		/*** 应用d3.zoom ***/
		this.applyZoom = function(){
			
			var _this = this;
			
			// 新增矩形，占据全部宽高，用来做操作触发层
			this.G.append("svg:rect")
				    .attr("class", "pane")
				    .attr("width", this.width)
				    .attr("height", this.height)
				    .call( d3.behavior.zoom().scaleExtent([1, 2]).on("zoom", function(){ _this.touchMove() }) );
			
		};
		
		
		/*** 展示某种血糖类型 ***/
		this.applyChangeType = function(){
			
			var sele = $(this.options.controller), _this = this;
			
			// select change
			sele.on('change', function(){
				
				var type = parseInt(this.value);
				
				_this.handleBaseData(type);
				
				var ran = CONFIG.getBdRange(type);
				
				// 更新Y轴血糖正常提示区间
				_this.container.find("rect.normal")
	                .attr("y", _this.glucoseData2Y(ran[2]))
	                .attr("height", _this.glucoseData2Y(ran[1]) - _this.glucoseData2Y(ran[2]));
	                
	            _this.baseType = type;
				
				_this.renderGraph();
			});
		};
		
		
		/*** 处理转屏事件 ***/
		this.applyOverTurn = function(){
			
			var _this = this;
			
			$(window).on('resize', function(){
				
				console.log('window resize');
				
				setTimeout(function(){
					
					_this.svgWidth = _this.container.width();
					
					// 计算图形区域的实际宽高度（除去坐标轴的区域）
					_this.width = _this.svgWidth - _this.options.padding[3];
					
					// 上面的this.width用来做展示，实际X轴占据的宽度要减去padding right
					_this.xWidth = _this.width - _this.options.padding[1];
					
					// 更新X轴的范围
					_this.x = d3.time.scale().range([0, _this.xWidth]);
					
					// 更新图形的尺寸
					_this.container.find('svg').attr('width', _this.svgWidth);
					_this.container.find('rect').attr('width', _this.width);
					_this.container.find("g.y .grid-line").attr("x2", _this.width);
					
					_this.renderGraph();
					
				}, 100);
				
			});
		};
		
		
		/*** 处理baseData，数据来源fullData ***/
		this.handleBaseData = function(type){
			
			var data = this.fullData;
			
			if( type === 0 ){ this.baseData = data; return; }
		
			var baseData = [];
			
			// 遍历baseData，取得showData
			for(var i=0; i<=data.length-1; i++){
				
				// 将对应type的数据push到baseData数组里
				if(data[i].type === type){
					baseData.push(data[i]);	
				}
	
			}
			
			this.baseData = baseData;
		};
		

		/*** 图形缩放至当前级别 ***/
		this.zoomToLevel = function(level){
	
			var xTime, lvTime;
			
			// 目前X轴中心位置对应的时间戳
			xTime = this.showStartTime + ( 1/2 * (this.showEndTime - this.showStartTime) );
			
			// 目标缩放级别的X轴时间戳总长度
			lvTime = this.xAxisDays[level] * 86400000;
			
			// 重新定位startTime和endTime
			this.showStartTime = xTime - ( lvTime / 2 );
			this.showEndTime = xTime + ( lvTime / 2 );
			
			this.level = level;
			
			// 绘制曲线
		    this.renderGraph();

		};
		
		
		/*** 处理touchmove事件 ***/
		this.touchMove = function(){		
			
			// 定义上一次平移量，初始为0
			this.transX = this.transX ? this.transX : 0;
			
			// 得出此次touchmove的移动量，大于0向右边滑动，小于0向左边滑动
			var moveX = d3.event.translate[0] - this.transX;
			
			// 当移动量大于1时，重绘图形
			//if( moveX > 1 || moveX < -1 ){
				
				// 1个X轴对应的毫秒数，86400000为1天的毫秒数
				var timeAll = this.xAxisDays[this.level] * 86400000,
					timeGap  = ( moveX / this.xWidth ) * timeAll;
				
				this.showStartTime = this.showStartTime - timeGap;
				this.showEndTime = this.showEndTime - timeGap;
				
				// 绘制曲线
		    	this.renderGraph();
		    	
			//}
			
			this.transX = d3.event.translate[0];

		};

		
		/*** 绘制曲线 ***/
		this.renderGraph = function(){
			
			// 如果graph已经存在了，移除它
			if( this.G.select("g.graph") ) this.G.select("g.graph").remove();
			
			// 如果X轴已经存在了，移除它
			if( this.G.select("g.x") ) this.G.select("g.x").remove();
			
			// 新增集合，放置所有需要变化的元素
			this.P = this.G.append("svg:g")
				    	.attr("class", "graph")
				    	.attr("clip-path", "url(#clip)");
			
			this.handleShowData();
			
		    // 绘制X轴
		    this.paintX();
		   
		    this.renderLine();
		    this.renderDots();
		   		    
		};
		
		
		/*** 处理当次展示所需要的showData，数据来源baseData ***/
		this.handleShowData = function(){
			
			var data = this.baseData;
			
			// 起始时间往前推1个轴的长度，结束时间往后推1个轴的长度
			// 这样绘制出来的曲线可以看到前后的延长
			var startTime = this.showStartTime - ( this.xAxisDays[this.level] * 86400000 ),
				endTime = this.showEndTime + ( this.xAxisDays[this.level] * 86400000 );
			
			var showData = [];
			
			// 遍历baseData，取得showData
			for(var i=0; i<=data.length-1; i++){
				
				// 当超出startTime时，开始push到showData数组里
				if(data[i].x >= startTime){
					showData.push(data[i]);	
				}
				// 当超出endTime时，跳出循环
				if(data[i].x >= endTime ){ 
					break;
				}
				
			}
			
			this.showData = showData;
			
			//console.log( "绘制数据时间跨度：" + new Date(startTime), new Date(endTime) )
		};
		
		
		/*** 绘制折线 ***/
		this.renderLine = function(){
			
			var _this = this, data = this.showData, ran = CONFIG.getBdRange(this.baseType);
			
			for (var s = 0; s < data.length - 1; s++) {
                var dataset = [];
                
                var obj_1 = new Object();
                obj_1.x = this.x( data[s].x );
                obj_1.y = data[s].y;
                obj_1.color = this.colorTran( data[s].value );
               

                var obj_2 = new Object();
                obj_2.x = this.x( data[s + 1].x );
                obj_2.y = data[s + 1].y;
                obj_2.color = this.colorTran( data[s + 1].value );
                
                // 保证X坐标不一样，否则会出现渐变色无法显示的BUG
                if( obj_2.x == obj_1.x ) obj_2.x = obj_2.x + 1;
                dataset.push(obj_1);
                dataset.push(obj_2);                
                
                //console.log( dataset[0], dataset[1])
                
                if (dataset[0].color == dataset[1].color) {
                    var color = obj_1.color;
                }else{
                	var lg = this.lineGradual(dataset[0], dataset[1], s);
                	var color = "url(#" + lg.attr("id") + ")";
                }
                
                //console.log( color );
                
                var line = d3.svg.line()
                				 .x(function(d) { return (d.x); })
                				 .y(function(d) { return (d.y); });

                this.P.append("path").datum(dataset).attr("class", "line")
                    .attr("d", function(d) {
                        return line(d);
                    }).style("fill", "#F00").style("fill", "none").style("stroke-width", 1).style("stroke", color);
            }
			
		};
		
		
		/*** 定义折线的渐变参数 ***/
		this.lineGradual = function(a1, b1, i){
			
			var defs = this.P.append("defs");
	        var id = "linearColor" + this.container.attr("id") + i;
	        var linearGradient = defs.append("linearGradient").attr("id", id)
	            .attr("x1", "0%").attr("y1", "0%").attr("x2", "100%").attr("y2", "0%");
	
	        var stop1 = linearGradient.append("stop").attr("offset", "0%")
	            .style("stop-color", a1.color);
	
	        var stop2 = linearGradient.append("stop").attr("offset", "100%")
	            .style("stop-color", b1.color);
	
	        return linearGradient;
			
		};
		
		
		/*** 绘制点 ***/
		this.renderDots = function() {

			var _this = this, data = this.showData;
			
			// 绘制展示的圆点
            this.P.selectAll(".series").data(data).enter().append("circle")
                .attr("class", "point").attr("r", function(d) {
                    if (_this.level == 1) return 4;
                    else return 3;
                }).attr("cx", function(d) {
                    if (d.y) return _this.x(d.x);
                }).attr("cy", function(d) {
                    if (d.y) return d.y;
                }).style("fill", function(d, i) {
                    return _this.colorTran(d.value);
                });
            
            // 绘制点击使用的圆点：点击展示血糖数据详情
            this.P.selectAll(".series").data(data).enter().append("circle")
                .attr("class", "point").attr("r", function(d) {
                    if (_this.level == 1) return 12;
                    else return 8;
                }).attr("cx", function(d) {
                    if (d.y) return _this.x(d.x);
                }).attr("cy", function(d) {
                    if (d.y) return d.y;
                }).style("fill", function(d, i) {
                    return _this.colorTran(d.value);
                }).style("fill-opacity", 0)
                .on('tap', function(d, i) {
                    _this.P.append("text")
	            	.attr("class", "mark")
	                .attr("x", _this.x(d.x))
	                .attr("y", d.y)
	                .attr("transform", "translate(7,3)")
	                .text(CONFIG.getBdName(d.type) +' [' + d.value + ']')
	                .style("fill", _this.colorTran(d.value))
                });
            
            // 日曲线横屏模式下展示所有血糖数据
            /*if( $(window).width() > $(window).height() && this.level == 1 ){
            
	            this.P.selectAll(".series").data(data).enter().append("text")
	            	.attr("class", "mark")
	                .attr("x", function(d) {
	                    if (d.y)
	                        return _this.x(d.x);
	                }).attr("y", function(d) {
	                    if (d.y)
	                        return d.y;
	                })
	                .attr("transform", "translate(7,3)")
	                .text(function(d){
	                	return CONFIG.getBdName(d.type) +'[' + d.value + ']';
	                })
	                .style("fill", function(d, i) {
	                    return _this.colorTran(d.value);
	                })
                
            }*/
        }
		
		
		/*** 绘制X坐标轴 ***/
		this.paintX = function(){

			// 定义X轴的展示范围
		    this.x.domain([ +this.showStartTime, +this.showEndTime]);

			// 定义X轴
			var xAxis = d3.svg.axis().scale(this.x).orient("bottom").tickPadding(0);

			// X轴刻度值定义
            xAxis.tickFormat(function(d) {
                return d3.time.format('%m-%d')(d);
            })
            
            // X轴刻度定义
            // 用d3.time.days来做间隔，会出现跨月份的BUG，如5月26到6月1日实际为6天，因为还有个5月31日
            //xAxis.ticks(d3.time.days, 7);
            xAxis.ticks(this.xAxisUnit[this.level]); 
            
            // 绘制X坐标轴
            var xscal = this.G.append("g").attr("class", "x axis").attr("transform",
                "translate(0," + this.height + ")").call(xAxis);
			
			// 绘制X坐标轴刻度拉伸的对照虚线
            this.G.selectAll("g.x g.tick").append("line").classed("grid-line", true).attr("x1", 0).attr("y1", 0).attr(
                    "x2", 0).attr("y2", -this.height);
                    
		};
		
		
		/*** 绘制Y坐标轴 ***/
		this.paintY = function(){
			
			// 插入初始Y轴坐标元素
			this.G.append("svg:g")
				    .attr("class", "y axis")
				    .attr("transform", "translate(" + 0 + ",0)");
			
			var yAxis = d3.svg.axis().scale(this.y).orient("left").tickPadding(0).outerTickSize(0);
			
			// Y轴坐标点
			var yAxisPoint = [0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 13.0, 16.0, 33.0]; 
			
			// 绘制Y坐标轴
			this.G.select("g.y.axis").call(yAxis)
				  .selectAll("text")
				  .text(function(d, i) {
				  	return parseFloat(yAxisPoint[i]).toFixed(1);
				  })
				  .select(".domain").style("stroke-width", 1);
			
			// 绘制Y坐标轴刻度拉伸的对照线
			this.G.selectAll("g.y g.tick")
				  .append("line")
				  .classed("grid-line", true)
				  .attr("x1", 0).attr("y1", 0)
				  .attr("x2", this.width).attr("y2", 0);
		};
		
		
		/*** 将血糖值映射到对应的Y坐标值 ***/
		this.glucoseData2Y = function(t) {
            var scale = this.height / 11; //11等分
            var h1 = scale;
            var h2 = scale * 7;
            var h3 = scale * 2;
            var h4 = scale;
            if (t < 3) {
                return h2 + h3 + h4 + h1 * (3 - t) / 3;
            } else if (t >= 3 && t < 10) {
                return h4 + h3 + (10 - t) * h2 / 7;
            } else if (t >= 10 && t < 16) {
                return h4 + (16 - t) * h3 / 6;
            } else {
                return (33 - t) * h4 / 17;
            }
        };
		
		
		/*** 颜色转换 1正常 2偏高 3偏低 ***/
	    this.colorTran = function(value) {
	    	
	    	var ran = CONFIG.getBdRange(this.baseType);
	    	
	    	//console.log( ran, value);
	    	
	        if (value <= ran[1]) {
	            return "red";
	        }
	        if (value <= ran[2]) {
	            return "#22AC38";
	        }
	        
	        return "orange";
	    };
	    
	}
	
    
    return GP
    
})