/* 
 * graph：曲线图模块
 * 说明：随机血糖不在此曲线图中展示（考虑到空间有限，只展示重要的数据）
 */


define(['/d3/d3','zepto'], function(require, exports, module){
	
	var GP = function(){
		
		this.defaultOptions = {
			
			container: "#md-graph",     // 需要插入SVG进行图形展示的元素
			padding: [20, 0, 30, 36]    // 图形的间距
		};
		
		this.fullData = null;     // 数据源：初始时拉取所有数据，之后的操作从这里进行数据计算，不再向服务器拉取
		this.baseData = null;     // 当前选择的时间段的数据（根据起始时间、结束时间从数据源获取）
		this.baseDataDays  = -1;  // 当前baseData所占时间宽度，以天为单位，-1代表全部
		
		this.showData = null;     // 当前展示所需的数据（根据X轴的区间从baseData中获取）
		this.showStartTime = 0;   // 当前展示图形的X轴的起始时间
		this.showEndTime = 0;     // 当前展示图形的X轴的结束时间
		
		this.init = function(options, data){
			
			// 扩展options
			this.options = $.extend(this.defaultOptions, options);
			this.container = $(this.options.container);
			
			this.svgWidth = this.container.width();
			this.svgHeight = this.container.height();
			
			// 添加CSS样式
			this.container.addClass('md-graph');
			
			// 计算图形区域的实际宽高度（除去坐标轴的区域）
			this.width = this.svgWidth - this.options.padding[1] - this.options.padding[3];
			this.height = this.svgHeight - this.options.padding[0] - this.options.padding[2];
			
			// 定义X轴、Y轴的缩放比
			this.x = d3.time.scale().range([0, this.width - 20]);
	    	this.y = d3.scale.linear().range([this.height, 0]).domain([0, 11]);
			
			this.initSVG();
			this.paintY();
			this.initGraph(data);
			
		};
		
		
		/*** 生成图形所需要的DOM元素 ***/
		this.initSVG = function(){
			
			// 绘制SVG和G
			this.G = d3.select(this.options.container)
						 .append('svg:svg')
						 .attr({ 'width':this.svgWidth, 'height':this.svgHeight })
						 .append("svg:g")
						 .attr("transform", "translate(" + this.options.padding[3] + "," + this.options.padding[0] + ")");
			
			
			this.G.append("svg:clipPath")
					.attr("id", "clip")
					.append("svg:rect")
					.attr("x", 0)
					.attr("y", 0)
					.attr("width", this.width)
					.attr("height", this.height);
					
			// 插入Y轴血糖正常提示区间
			this.G.append("rect").attr("class", "normal")
					.attr("x", 0)
	                .attr("y", this.glucoseData2Y(8)).attr("width", this.width)
	                .attr("height", this.glucoseData2Y(4.0) - this.glucoseData2Y(8)).style("fill", "#00A0E9");
			
			// 新增矩形，占据全部宽高，用来做操作触发层
//			this.svg.append("svg:rect")
//				    .attr("class", "pane")
//				    .attr("width", this.width)
//				    .attr("height", this.height)
//				    .call( d3.behavior.zoom().x(this.x).scaleExtent([1, 8]).on("zoom", zoom) );
			    
		};
		
		
		/*** 初始：加载数据、绘制曲线 ***/
		this.initGraph = function(data){
			
			var _this = this;
			
		    // 对数据进行格式处理
		    data.forEach(function(d) {
		        d.x = +(d.date*1000);                  // X坐标值转换为时间戳，因Unix时间戳单位为秒，这里*1000转换为毫秒
		        d.y = +_this.glucoseData2Y(d.value);   // Y坐标值转换为对应的值
		        d.color = +d.status;                   // 颜色参数：1代表正常，2代表偏高，3代表偏低
		    });
			    
		    // 将数据集合顺序进行倒置，方便之后对其进行操作
		    var newData = new Array();
	        if (data) {
	            for (var j = data.length - 1; j >= 0; j--) {
	                newData.push(data[j]);
	            }
	        }
		    
		    // 存储数据
		    this.fullData = newData;
		    this.baseData = newData;
			
			// 绘制曲线
		    this.renderGraph(newData);
            
		};
		
		
		/*** 绘制曲线 ***/
		this.renderGraph = function(data){
			
			// 新增集合，放置所有需要变化的元素
			this.P = this.G.append("svg:g")
				    	.attr("class", "graph")
				    	.attr("clip-path", "url(#clip)");
			
			// 定义X轴的展示范围
		    this.x.domain([ +new Date(2016, 5, 26, 0).getTime(), +new Date(2016, 6, 13, 24).getTime()]);
		    
		    // 绘制X轴
		    this.paintX( this.baseDataDays );
		   
		    this.renderLine(data);
		    this.renderDots(data);
		   		    
		};
		
		
		/*** 计算时间跨度 ***/
		this.handleShowTime = function(){
			
			
		};
		
		
		/*** 处理当次展示所需要的showData，数据来源baseData ***/
		this.handleShowData = function(){
			
			var data = this.baseData;
			
			
		};
		
		
		/*** 绘制折线 ***/
		this.renderLine = function(data){
			
			var _this = this;
			
			for (var s = 0; s < data.length - 1; s++) {
                var dataset = [];
                
                var obj_1 = new Object();
                obj_1.x = this.x( data[s].x );
                obj_1.y = data[s].y;
                obj_1.color = data[s].color;
               

                var obj_2 = new Object();
                obj_2.x = this.x( data[s + 1].x );
                obj_2.y = data[s + 1].y;
                obj_2.color = data[s + 1].color;
                
                // 保证X坐标不一样，否则会出现渐变色无法显示的BUG
                if( obj_2.x == obj_1.x ) obj_2.x = obj_2.x + 1;
                dataset.push(obj_1);
                dataset.push(obj_2);                
                

                var lg = this.lineGradual(dataset[0], dataset[1], s);
                var color = "url(#" + lg.attr("id") + ")";
                
                if (dataset[0].color == dataset[1].color) {
                    color = this.colorTran(dataset[0].color);
                }
                
                var line = d3.svg.line()
                				 .x(function(d) { return (d.x); })
                				 .y(function(d) { return (d.y); });

                this.P.append("path").datum(dataset).attr("class", "line")
                    .attr("d", function(d) {
                        return line(d);
                    }).style("fill", "#F00").style("fill", "none").style("stroke-width", 1).style("stroke", color);
            }
			
		};
		
		
		/*** 定义折线的渐变参数 ***/
		this.lineGradual = function(a1, b1, i){
			
			var defs = this.P.append("defs");
	        var id = "linearColor" + this.container.attr("id") + i;
	        var linearGradient = defs.append("linearGradient").attr("id", id)
	            .attr("x1", "0%").attr("y1", "0%").attr("x2", "100%").attr("y2", "0%");
	
	        var stop1 = linearGradient.append("stop").attr("offset", "0%")
	            .style("stop-color", this.colorTran(a1.color));
	
	        var stop2 = linearGradient.append("stop").attr("offset", "100%")
	            .style("stop-color", this.colorTran(b1.color));
	
	        return linearGradient;
			
		};
		
		
		/*** 绘制点 ***/
		this.renderDots = function(data) {

			var _this = this;
			
            this.P.selectAll(".series").data(data).enter().append("circle")
                .attr("class", "point").attr("r", function(d) {
                    if (d.y)
                        return 3;
                }).attr("cx", function(d) {
                    if (d.y)
                        return _this.x(d.x);
                }).attr("cy", function(d) {
                    if (d.y)
                        return d.y;
                }).style("fill", function(d, i) {
                    return _this.colorTran(d.color);
                }).style("cursor", "pointer").on("click", function(d, i) {
                    d3.select(this).attr("r", 5);
                })
            
        }
		
		
		/*** 绘制X坐标轴 ***/
		this.paintX = function(days){
			
			// 如果X轴已经存在了，移除它
			if( this.G.select("g.x") ) this.G.select("g.x").remove();
			
			// 定义X轴
			var xAxis = d3.svg.axis().scale(this.x).orient("bottom").tickPadding(0);

			// X轴刻度值定义
            xAxis.tickFormat(function(d) {
                switch (days) {
                    case 1:
                    	 console.log(d)
                        if (d.getHours() == 0) {
                            return d3.time.format('%m-%d')(d);
                        } else {
                            return d3.time.format('%H:%M')(d);
                        } 
                    default:
                        return d3.time.format('%m-%d')(d);
                }
            })
            
            // X轴刻度定义
            if( days == 1 ) xAxis.ticks(d3.time.hour, 4);
            else if( days > 1 && days <=7 ) xAxis.ticks(d3.time.day, 1);
            else if( days > 7 && days <=14 ) xAxis.ticks(d3.time.day, 2);
            else xAxis.ticks(d3.time.day, 5);
            
            // 绘制X坐标轴
            var xscal = this.G.append("g").attr("class", "x axis").attr("transform",
                "translate(0," + this.height + ")").call(xAxis);
                
//          if ( days == 1 ) {
//              var sx = ["", "空腹", "早后", "午前", "午后", "晚前", "晚后", "睡前", "凌晨", ""];
//              xscal.selectAll("text").text(function(d, i) {
//                  return sx[i]
//              });
//          }
			
			// 绘制X坐标轴刻度拉伸的对照虚线
            this.G.selectAll("g.x g.tick").append("line").classed("grid-line", true).attr("x1", 0).attr("y1", 0).attr(
                    "x2", 0).attr("y2", -this.height);
                    
		};
		
		
		/*** 绘制Y坐标轴 ***/
		this.paintY = function(){
			
			// 插入初始Y轴坐标元素
			this.G.append("svg:g")
				    .attr("class", "y axis")
				    .attr("transform", "translate(" + 0 + ",0)");
			
			var yAxis = d3.svg.axis().scale(this.y).orient("left").tickPadding(0).outerTickSize(0);
			
			// Y轴坐标点
			var yAxisPoint = [0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 13.0, 16.0, 33.0]; 
			
			// 绘制Y坐标轴
			this.G.select("g.y.axis").call(yAxis)
				  .selectAll("text")
				  .text(function(d, i) {
				  	return parseFloat(yAxisPoint[i]).toFixed(1);
				  })
				  .select(".domain").style("stroke-width", 1);
			
			// 绘制Y坐标轴刻度拉伸的对照线
			this.G.selectAll("g.y g.tick")
				  .append("line")
				  .classed("grid-line", true)
				  .attr("x1", 0).attr("y1", 0)
				  .attr("x2", this.width).attr("y2", 0);
		};
		
		
		/*** 将血糖值映射到对应的Y坐标值 ***/
		this.glucoseData2Y = function(t) {
            var scale = this.height / 11; //11等分
            var h1 = scale;
            var h2 = scale * 7;
            var h3 = scale * 2;
            var h4 = scale;
            if (t < 3) {
                return h2 + h3 + h4 + h1 * (3 - t) / 3;
            } else if (t >= 3 && t < 10) {
                return h4 + h3 + (10 - t) * h2 / 7;
            } else if (t >= 10 && t < 16) {
                return h4 + (16 - t) * h3 / 6;
            } else {
                return (33 - t) * h4 / 17;
            }
        };
		
		
		/*** 颜色转换 1正常 2偏高 3偏低 ***/
	    this.colorTran = function(color) {
	        if (color == 3) {
	            return "red";
	        }
	        if (color == 1) {
	            return "#22AC38";
	        }
	        if (color == 2) {
	            return "orange";
	        }
	        return "orange";
	    };
	    
	}
	
    
    return GP
    
})