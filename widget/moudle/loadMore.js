/* 
 * loadMore：加载更多模块
 */

define(['zepto'], function(require, exports, module){
	
	/* 页面滚动 */
	
	var output = function(){
		
		this.scroller = null;
		this.container = null;
		this.dataUrl = null;
		this.refresh = null;
		this.lastId = 0;
		
		this.init = function(scrollerID, contID, dataURL){
			
			this.container = $("#" + contID);
			this.dataUrl = dataURL.replace(/&amp;/ig,"&");
			
			this.initScroll(scrollerID);
			this.initRefresh();
			
			// 初始加载
			this.refresh.icon.attr('class', 'ui-loading');
			this.refresh.txt.html('加载中...');
			loadData(this, this.refresh);
			
			// 刷新scroller
			var that = this;
			setTimeout(function(){
				that.scroller.refresh();
			}, 350);
			
		}
		
		this.initScroll = function(id){
			
			var pageScroll = require('/widget/moudle/pageScroll');
			// 因为要监测上拉加载更多，必须加上probeType参数进行监测，这里设置为2减少性能消耗
			var pageScrollItem = new pageScroll(id, {bounce: true, probeType: 2});
			
			this.scroller = pageScrollItem.scroller;
		};
		
		this.initRefresh = function(){
			
			var refresh = require('/widget/moudle/refreshMore');
			this.refresh = new refresh(this.container.parent(), this.scroller, loadData, this);
			
		};
		
		this.makeItem = function(data){
			
			
		}
		
	}
	
	
	// 加载数据
	// 因为涉及到多个闭包和环境，将loadData独立出来
	var loadData = function(obj, refresh){
		
		$.getJSON( obj.dataUrl, { lastId: obj.lastId }, function(data){
			
			//data = null;
			
			// 如果返回的数据为空
			if( data == "" || data == null ){
				
				if( refresh != null && refresh != 'undefined') refresh.die();

				obj = null;
				return false;
			}
			
			// JSON数据类型
			// type: 1
			// [{id:1},{id:2},{id:3}]
			// type: 2
			// [data:[{id:1},{id:2},{id:3}], lastId:3]
			var type = 1;
			
			// 数据源
			var Data;
			
			// 判断JSON格式，处理数据和lastId
			if( typeof(data.lastId) != 'undefined' ){
				
				Data = data.data;
				type = 2;
				
			}else{
				
				Data = data
			}
			
			for(var i=0; i<=Data.length-1; i++){
			
				var htmlx = obj.makeItem(Data[i], i);
				obj.container.append( htmlx );
			}
			
			// 刷新scroller：这里有点诡异，有时候好像没起到作用，所以不得不多处刷新
			//setTimeout(function(){
				obj.scroller.refresh();
			//}, 300);
			
			// 如果有返回lastId而且值为-1：也就是提示已经没有数据了
			if( type == 2 && data.lastId == -1 ){
				
				if( refresh != null && refresh != 'undefined') refresh.die();

				obj = null;
				return false;
			}
			
			// 还原下拉刷新状态
			if( refresh != null && refresh != 'undefined') refresh.backHtml();
			

			// 记录最后一条数据的ID
			if(type == 1) obj.lastId = data[i-1].id;
			else obj.lastId = data.lastId;
			
		});
	}
	
	exports.output = output
	
});