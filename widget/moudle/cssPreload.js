/* 
 * cssPreload：页面CSS预加载
 * Mobilebone页面样式提前加载
 */


define(['/widget/base/base'], function (require, exports, module) {
	
	var cssPreload = {
		
		// 页面CSS资源表
		// 这里针对的是需要Ajax请求加载的页面
		// 但不包括同区域内的页面，比如知识库列表页、文章页样式和知识库首页放在一块，没有必要再加载
		// 需要考虑到页面刷新的情况，比如从A进入B（B页面加载C页面样式），在进入C，如果此时直接刷新页面，将直接从A进入C，样式则无加载了。
		// 解决这个问题有两个方法：
		// 1、将B页面和C页面样式写在一个文件里；
		// 2、C页面加载时判断来源，如果不是正常的，则加载样式（或者简单粗暴C页面JS里面再发起一次样式加载请求）
		source: {

			'bloodAdd': ["/page/bloodAdd/bloodAdd.css"],
			'bloodPost': ["/page/bloodAdd/bloodAdd.css"],
			'viewComment': ["/page/view/view.css"],
			'viewBlood': ["/static/css/swiper.min.css", "/page/view/view.css"],
			'viewMood': ["/page/view/view.css"],
			'find': ["/static/css/swiper.min.css", "/page/find/find.css"]
		},
		
		// 页面后置关系：从A页面发起Ajax请求加载B页面，那么B页面就是A页面的后置。
		after: {
			
			// 首页
			'index': ['bloodAdd', 'viewComment', 'find'],
			// 血糖添加页面
			'bloodAdd': ['bloodPost'],
			// 消息中心评论页面
			'ucenterNewsComment': ['viewComment', 'viewMood'],
			// 消息中心点赞页面
			'ucenterNewsLike': ['viewMood']
		},
		
		// 第三层页面前置关系：首页、糖友圈、知识库、用户中心这四个是第一层页面，它们发起Ajax请求加载的页面为第二层，从第二层再发起请求加载的为第三层。
		// 第三层的页面在直接刷新的时候，不会加载第二层，而是直接从第一层加载第三层，这个时候就需要补充加载第三层的CSS样式了（原本是第二层页面JS加载的）。
		front: {
			
			// 1.血糖添加页面bloodPost：跟第二层页面bloodAdd是同一个样式表，无需处理
			// 2.我的关注、我的粉丝页面：同样无需处理
			
			
		},
		
		// 预加载样式文件
		preload: function(page){
			
			var afterArr = this.after[page];
			for(var i=0; i<afterArr.length; i++){
				
				var cssSrc = this.source[ afterArr[i] ];
				
				// 加载后置页面的样式文件
				// 因为这里FIS无法替换css模块ID名称，所以在fis-conf.js里面配置css模块名字为固定的"css"
				for(var k=0; k<cssSrc.length; k++) require(["css!"+ CONFIG.ROOT + cssSrc[k] + ""]);
				
			}
			
		},
		
		// 根据页面ID，预加载样式表
		init: function(page){
			
			switch(page)
			{
				case 'index':
					this.preload(page);
					break;
					
				case 'ViewUcenter':
					this.preload(page);
					break;
					
				case 'ucenterNewsComment':
					this.preload(page);
					break;
				
				case 'ucenterNewsLike':
					this.preload(page);
					break;
				
				
				default:
					
			}
		}
		
	}
	
	return cssPreload;
	
});