/* 
 * loadMore_index：加载更多模块
 * @ 首页专用
 */

define(['zepto'], function(require, exports, module){
	
	/* 页面滚动 */
	
	var output = function(){
		
		this.scroller = null;
		this.container = null;
		this.dataUrl = null;
		this.refresh = null;
		this.tkkLastId = 0;
		this.currentPage = 0;
		
		this.init = function(scrollerID, contID, dataURL){
			
			this.container = $("#" + contID);
			this.dataUrl = dataURL.replace(/&amp;/ig,"&");
			
			this.initScroll(scrollerID);
			this.initRefresh();
			
			// 初始加载
			this.refresh.icon.attr('class', 'ui-loading');
			this.refresh.txt.html('加载中...');
			loadData(this, this.refresh);
			
			if( tbb.from == 'app' ) this.reloadPage();
			
			// 刷新scroller
			var that = this;
			setTimeout(function(){
				that.scroller.refresh();
			}, 350);
			
		}
		
		this.initScroll = function(id){
			
			var pageScroll = require('/widget/moudle/pageScroll');
			// 因为要监测上拉加载更多，必须加上probeType参数进行监测，这里设置为2减少性能消耗
			var pageScrollItem = new pageScroll(id, {bounce: true, probeType: 2});
			
			this.scroller = pageScrollItem.scroller;
		};
		
		this.initRefresh = function(){
			
			var refresh = require('/widget/moudle/refreshMore');
			this.refresh = new refresh(this.container.parent(), this.scroller, loadData, this);
			
		};
		
		this.makeItem = function(data){
			
			
		};
		
		// 上拉重新加载页面（仅APP使用）
		this.reloadPage = function(){
			
			var t = $(['<div class="ui-refresh-up mt13">',
				'<span class="ui-refresh-icon"></span>',
				'<span class="ui-refresh-label">下拉刷新...</span>',
				'</div>'].join(""));
			
			this.container.parent().prepend(t);
			
			var that = this;
			that.icon = t.find('.ui-refresh-icon'),
			that.txt  = t.find('.ui-refresh-label');
			
			this.scroller.on('scroll', function () {
			
				if( that == null ) return;
	
				var	ele  = that.scroller,
					icon = that.icon,
					txt  = that.txt;
				
				if (ele.y > (ele.minScrollY + 40) && !icon.hasClass('ui-refresh-flip')) {
					
					icon.addClass('ui-refresh-flip');
					txt.html('松手开始刷新...');
					
					ele.minScrollY = 40;
				
				// 顶部下拉松开时如果小于30的话，取消加载动作
				} else if (ele.y < 40 && icon.hasClass('ui-refresh-flip')) {
					
					icon.removeClass('ui-refresh-flip');
					txt.html('下拉刷新...');
					
					ele.minScrollY = 0;
				}
			    
			});
			
			// 动作效果结束后
			this.scroller.on('scrollEnd', function () {	
				
				if( that == null ) return;
				
				var	ele  = that.scroller,
					icon = that.icon,
					txt  = that.txt;
					
				if (icon.hasClass('ui-refresh-flip')) {
					
					icon.attr('class', 'ui-loading');
					txt.html('加载中...');
					
					// 因为是刷新，所以取最新的数据
					that.tkkLastId = 0;
					that.currentPage = 0;
					loadData(that, that.refresh, 'appUp');
				}
				
			});
		}
		
	}
	
	
	// 加载数据
	// 因为涉及到多个闭包和环境，将loadData独立出来
	var loadData = function(obj, refresh, appUp){
		
		// 定义传输数据
		var dataPost = {

			tkkLastId: obj.tkkLastId,
			currentPage: obj.currentPage + 1,
			uid: IndexUserId
		}
		
		$.getJSON( obj.dataUrl, $.param(dataPost), function(data){
			
			//data = null;
			
			// 如果返回的数据为空
			if( data == "" || data == null ){
				
				if( refresh != null && refresh != 'undefined') refresh.die();

				obj = null;
				return false;
			}
			
			// 数据源
			var Data = data.items;
			
			// 如果是APP，下拉刷新
			if( appUp ) obj.container.empty();
			
			for(var i=0; i<=Data.length-1; i++){
			
				var htmlx = obj.makeItem(Data[i], i);
				obj.container.append( htmlx );
			}
			
			// 刷新scroller
			obj.scroller.refresh();

			// 如果有返回lastId而且值为-1：也就是提示已经没有数据了
			if( data._meta.tkkLastId == -1 ){
				
				if( refresh != null && refresh != 'undefined') refresh.die();

				obj = null;
				return false;
			}
			
			// 还原下拉刷新状态
			if( refresh != null && refresh != 'undefined') refresh.backHtml();
			
			if( appUp ){
				
				// 重置下拉刷新
				obj.icon.attr('class', 'ui-refresh-icon');
				obj.txt.html('下拉刷新...');
			}
			
			
			// 记录tkkLastId和currentPage
			obj.tkkLastId = data._meta.tkkLastId;
			obj.currentPage = data._meta.currentPage;
			
		});
	}
	
	exports.output = output
	
});