/* 
 * pageScroll：页面滚动
 * 判断内容是否超出容器高度，如超出，则设置Iscroll滚动
 */


define(function (require, exports, module) {
	
	// 加载需要用到的插件
	require('iscroll');
	require('zepto');
	
	// 定义模块
	// 参数解释
	// id：需要检测高度和设置滚动的容器
	// options：需要设置的IScroll参数集，如为空或者false，则使用默认的参数进行设置
	var pageScroll = function(id, options, func){
		
		// iScroll默认配置参数
		this.opts = {
			
			click: true,
			tap: true,          // click 和 tap 都设置为false，因为都用 fastclick 来替代了。
			scrollbars: true,
			fadeScrollbars: true,
			vScrollbar: false,   // 禁止横向滚动
			bounce: false,       // 取消边界反弹
			deceleration: 0.003  // 速度系数
		};
		
		this.scroller = null;
		this.ele = null;
		
		this.init = function(id, options, func){
			
			this.ele = document.getElementById(id);
			
			// 如果options有值且不为false
			if( options ) this.opts = $.extend(this.opts, options);
			
			// 如果func参数为函数的话
			if( typeof(func) == 'function' ) func();
				
			// 如果页面内容超出，设置滚动
			this.buildScroll();
		
		};
		
		// 设置页面元素IScroll
		this.buildScroll = function(){
			
			var ele = this.ele;
			
			// 如果页面内容超出，设置滚动
			//if( ele.scrollHeight > ele.offsetHeight){
				//console.log(this.opts)
				this.scroller = new IScroll( ele, this.opts );
			//}
		},
		
		// 将刷新函数提供出去
		this.refresh = function(){
			
			if( this.scroller == null ) this.buildScroll();
			else this.scroller.refresh();
			
		};
		
		// 初始化
		this.init(id, options, func);
		
	}
	
	// 输出模块
	return pageScroll;

});