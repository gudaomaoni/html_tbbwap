/* 
 * refreshMore：加载更多模块
 * 初始化HTML、处理页面滚动事件
 */

define(function (require, exports, module) {
	
	// 加载需要用到的插件
	require('iscroll');
	require('zepto');
	
	// 定义模块
	// 参数解释
	// scroller：已经生成的IScroll元素
	// container：需要插入上拉刷新HTML代码的元素
	// loadFunc：上拉执行的事件
	// obj：目标Object（生成此refresh的目标Object，把指针传过来，等会进行回传处理）
	var refresh = function(container, scroller, loadFunc, obj){
		
		// 设置min-height:101%，内容高度没有超过页面的情况下也能滚动
		scroller.scroller.style.minHeight = "101%";
		
		this.scroller = scroller;
		
		var t = $(['<div class="ui-refresh-down mt13">',
				'<span class="ui-refresh-icon"></span>',
				'<span class="ui-refresh-label">上拉加载更多...</span>',
				'</div>'].join(""));
		
		container.addClass('ui-refresh').append( t );
		
		this.icon = t.find('.ui-refresh-icon');
		this.txt  = t.find('.ui-refresh-label');
		
		var that = this;
		
		this.scroller.on('scroll', function () {
			
			if( that == null ) return;

			var	ele  = that.scroller,
				icon = that.icon,
				txt  = that.txt;
				
			// 如果正在加载中，不做处理
			if( icon.hasClass('ui-loading') ) return;
			
			if (ele.y < (ele.maxScrollY - 30) && !icon.hasClass('ui-refresh-flip')) {
				
				icon.addClass('ui-refresh-flip');
				txt.html('松手开始更新...');
			
			// 底部上拉松开时如果小于0的话，取消加载动作
			} else if (ele.y > (ele.maxScrollY) && icon.hasClass('ui-refresh-flip')) {
				
				icon.removeClass('ui-refresh-flip');
				txt.html('上拉加载更多...');
			}
		    
		});
		
		// 动作效果结束后
		this.scroller.on('scrollEnd', function () {	
			
			if( that == null ) return;
			
			var	ele  = that.scroller,
				icon = that.icon,
				txt  = that.txt;
				
			if (icon.hasClass('ui-refresh-flip')) {
				
				icon.attr('class', 'ui-loading');
				txt.html('加载中...');
				
				// 执行传进来的函数
				if( typeof(loadFunc) == 'function' ) loadFunc(obj, that);
			}
			
		});
		
		this.scroller.on('scrollStart', function () {	
			
			if( that == null ) return;
			
			that.scroller.refresh()
			
		});
		
		// 加载完成之后执行
		// 因为涉及到数据交互，只能在外部进行数据处理，然后调用这个方法来进行样式修改了
		this.backHtml = function(){
			
			var	ele  = that.scroller,
				icon = that.icon,
				txt  = that.txt;
					
			// Ajax请求数据进行刷新
			setTimeout(function(){
				
				// 更新Swiper和iScroll高度
				ele.refresh();
				
				// 重置上拉加载
				icon.attr('class', 'ui-refresh-icon');
				txt.html('上拉加载更多...');
				
			}, 200);
		};
		
		// 没有数据可以加载了的时候清空refresh object
		this.die = function(){
			
			var	ele  = that.scroller,
				icon = that.icon,
				txt  = that.txt;
				
			// 前台显示：没有更多内容了
			icon.attr('class', '');
			txt.html('<span style="color:#999">没有更多内容了</span>');
			
			// 删除绑定的事件
			//ele._events.scroll = null;
			//ele._events.scrollEnd = null;
			//ele._events.scrollStart = null;
			// 因为滚动条也绑定在事件里，所以换一种方式，在事件里判断了
			
			// 释放refresh对象
			that = null;
			
		}
		
		//if( obj.container.children().length == 0 ) this.die();
		
	}
	
	// 输出模块
	return refresh;

});