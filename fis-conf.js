// AMD配置项
// npm install [-g] fis3-hook-amd
fis.hook('amd', {
  urlArgs: 'v=161022',
  paths: {
    zepto: '/static/lib/zepto.min',
    fastclick: '/static/lib/fastclick',
    swiper: '/static/lib/swiper.min',
    headroom: '/static/lib/headroom',
    iscroll: '/static/lib/iscroll-probe',
    iscroll4: '/static/lib/iscroll4',
    mobilebone: '/static/lib/mobilebone',
    css: '/widget/moudle/css',
    raphael: '/static/lib/raphael-2.1.4.min',
    justgage: '/static/lib/justgage',
    datePicker: '/static/lib/datePicker',
    chart: '/static/lib/chart.min',
    photoswipe: '/static/lib/photoswipe.min',
    photoswipeUI: '/static/lib/photoswipe-ui-default.min',
    base: '/widget/base/base'
  },
  shim: {
    "*": {
      deps: ["fastclick"] // 这一句好像没生效..
    },
    "gmu": {
      deps: ["zepto"]
    },
    "datePicker": {
      deps: ["zepto", "iscroll4"]
    }
  }
});

// 设置 css组件的固定ID为css
// cssPreload.js里面会用到
fis.match('/widget/moudle/css.js', {
  isMod: true,
  id: 'css'
});

// 设置 widget 下的所有JS文件都是组件
fis.match('/widget/**/*.js', {
  isMod: true,
  packTo: '/static/pkg.js',
  optimizer: fis.plugin('uglify-js')
});

// 设置 page 下的所有JS文件都是组件
fis.match('/page/**/*.js', {
  isMod: true,
  packTo: '/static/page.js?v=20161022',
  optimizer: fis.plugin('uglify-js')
});

// 分析 __RESOURCE_MAP__ 结构，来解决资源加载问题
// npm install [-g] fis3-postpackager-loader
fis.match('::package', {
  postpackager: fis.plugin('loader', {
    resourceType: 'amd',
    sourceMap: true, //是否生成依赖map文件
    useInlineMap: true // 资源映射表内嵌
  })
});

// 启用 fis-spriter-csssprites 插件
fis.match('::package', {
  spriter: fis.plugin('csssprites')
});

// 对 CSS 进行图片合并
fis.match('*.css', {
  // 给匹配到的文件分配属性 `useSprite`
  useSprite: true
});

// 发布配置
fis.media('pack')
  .match('::package', {
    spriter: fis.plugin('csssprites')
  })
  .match('*.{js,css,png}', {
    //useHash: true   // 加 md5
  })
  .match('/static/lib/*.js', {
    optimizer: fis.plugin('uglify-js')
  })
  .match('*.css', {
    optimizer: fis.plugin('clean-css'),
    useSprite: true
  })
  .match('/static/css/*.css', {
    packTo: '/static/lib.css'
  })
  .match('/page/**.css', {
    packTo: '/static/page.css'
  })
  .match('/widget/**.css', {
    packTo: '/static/widget.css'
  })
  .match('*.png', {
    optimizer: fis.plugin('png-compressor')
  })
  .match('*', {
    release: 'template/cis_app/tkk/assets/$0'
  });