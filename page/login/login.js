define(['zepto', 'swiper'],function(){
	
	// 调用Swiper插件
	var loginSwiper = new Swiper('#loginSwiper', {
		effect:'flip',
		initialSlide: 0,
		onlyExternal: true,
	    speed: 300,
	    spaceBetween: 0,
	    autoHeight: true
	});
	
	var $title = $("#login_title");
	
	// 老用户登录按钮
	$("#login_old").on('tap', function(){
		
		loginSwiper.slideTo(1);
		$title.html('老用户登录');
		
	});
	
	// 新用户登录按钮
	$("#login_new").on('tap', function(){
		
		//加载层
		layer.open({type: 2});
		
		$.get(loginNameCheckURL, function(response){
			
			var data = eval("(" + response + ")");
			
			// 如果微信昵称已经被注册用户名了
			if( data.exist == true ){
				
				layer.closeAll();
				
				loginSwiper.slideTo(2);
				$title.html('新用户注册');
				$("#new_name").trigger('focus');
			
			}else if( data.url != "" ){
				
				// AJAX 提交操作
				$.post( data.url, function(response){
					
					layer.closeAll();
					
					var data = eval("(" + response + ")");
					
					if( data.success == true ){
						
						window.location.href = data.toUrl;
					
					}else{
						
						alert('出错啦！请与工作人员进行联系~');
					}
					
				});
				
			}else{
				
				layer.closeAll();
				
				alert(response)
			}
			
		});
		
	});
	
	// 返回按钮
	$(".imui_icon_back").on('tap', function(){
		
		loginSwiper.slideTo(0);
		$title.html('登录糖帮帮');
		return false
	});
	
	// 忘记密码
	$(".login-forget").on('click', function(){
		
		layer.open({
		    title: '提示信息',
		    style: 'text-align:left',
		    content: '如忘记密码，请在电脑上登录糖帮帮进行修改，网址如下：<br />http://www.tangbangbang.cn/。'
		});
		
	});
	
	// 老用户登录确认
	$("#login_submit_old").on('click', function(){
		
		if( $("#old_name").val() == "" ){
			
			layer.open({
			    content: '请输入您的用户名！',
			    className: 'layerInfo',
			    time: 3,
			    end: function(){
			    	$("#old_name").trigger('focus')
			    }
			});
			
			return false;
			
		}
		
		if( $("#old_code").val() == "" ){
			
			layer.open({
			    content: '请输入您的密码！',
			    className: 'layerInfo',
			    time: 3,
			    end: function(){
			    	$("#old_code").trigger('focus')
			    }
			});
			
			return false;

		}
		
		//加载层
		layer.open({ content: '正在登录中，请稍候...' });
		
		// AJAX 提交数据
		var postURL = $('#old_form').attr('action');
		var postData = {
			name: $("#old_name").val(),
			code: $("#old_code").val()
		}
		
		// AJAX 提交操作
		$.post( postURL, postData, function(response){
			
			layer.closeAll();
			
			var data = eval("(" + response + ")");
			
			if( data.success == true ){
				
				window.location.href = data.toUrl;
			
			}else{
				
				alert('出错啦！请与工作人员进行联系~');
			}
			
		});
		
		return false
		
	});
	
	// 新用户登录确认
	$("#login_submit_new").on('click', function(){
		
		if( $("#new_name").val() == "" ){
			
			layer.open({
			    content: '请输入您的用户名！',
			    className: 'layerInfo',
			    time: 3,
			    end: function(){
			    	$("#new_name").trigger('focus')
			    }
			});
			
			return false;
		}
		
		//加载层
		layer.open({ content: '正在注册登录中，请稍候...' });
		
		// AJAX 提交数据
		var postURL = $('#new_form').attr('action');
		var postData = {
			name: $("#new_name").val()
		}
		
		// AJAX 提交操作
		$.post( postURL, postData, function(response){
			
			layer.closeAll();
			
			var data = eval("(" + response + ")");
			
			if( data.success == true ){
				
				window.location.href = data.toUrl;
			
			}else{
				
				alert('出错啦！请与工作人员进行联系~');
			}
			
		});
		
		return false
		
	});
	

});