define(['base', 'swiper', '/widget/utils/dateUtil'],function(){
	
	// 参数定义
	var IndexSwiper,
		IndexSwiper2,
		IndexSwiper3,
		loadedIndexArea2;
		
	
	/*
	 * @ reload加载部分（局部事件绑定，数据加载）
	 */
	var init = function(){
	
		// 初始加载广场数据
		loadIndexArea("indexCont", "indexData", IndexAreaUrl[0].more);
		
		// 关注区域初始数据加载标识
		loadedIndexArea2 = false;
		
		// 调用Swiper插件
		IndexSwiper = new Swiper('#indexSwiper', {
			initialSlide: 0,
			onlyExternal: true,
			iOSEdgeSwipeDetection : true,
			pagination: '#indexSwiperPage',
			paginationType: 'custom', /* 自定义模式 */
			paginationCustomRender: function (swiper, current, total) {
				
				// 更换分页当前样式
			    $("#indexSwiperPage li").removeClass('a').eq(current-1).addClass('a');

			},
		    speed: 400,
		    spaceBetween: 10
		});
		
		
		// 调用Swiper插件
		IndexSwiper2 = new Swiper('#indexSwiper2', {
			loop:true,
			pagination: '.swiper-pagination',
			iOSEdgeSwipeDetection : true,
		    speed: 400,
		    autoplay: 6000
		});
		
		// 调用Swiper插件
		IndexSwiper3 = new Swiper('#indexSwiper3', {
			loop:true,
			pagination: '.swiper-pagination',
			iOSEdgeSwipeDetection : true,
		    speed: 400,
		    autoplay: 6000
		});
		

	};
	
	
	/*
	 * @ 全局事件绑定
	 */
	
	/* 点击进行切换 广场、关注 */
	$(document).on('click', '#indexSwiperPage li', function(){
		
		var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
		IndexSwiper.slideTo(CurrentPos);
		
		// 第一次进入关注区域时加载初始数据
		if( CurrentPos == 1 && !loadedIndexArea2 ){
			loadIndexArea("indexCont2", "indexData2", IndexAreaUrl[1].more);
			loadedIndexArea2 = true;
		}
		
		// 更改页面数据缓存
		var tabid=$('.body_main').attr('id');
		var state = {
			url: window.location.href,
			tabid: tabid,
			html: $('body').html(),
		};
		history.replaceState(state,null,window.location.href);
		
	});
	
	
	/* 点击图片查看大图 */
	var PhotoSwipe = require('photoswipe'),
		photoswipeUI = require('photoswipeUI'),
		pswpElement = $("body").find('.pswp').get(0);;
	
	$(document).on('click', '#page_index .ui-moodPhoto a', function(){
		
		layer.open({type: 2});
		
		// define options (if needed)
	    var options = {
			
			shareEl: false,     // 隐藏分享按钮
			counterEl: false,   // 隐藏计数按钮
	        history: false,     // 不加入历史记录 
	        fullscreenEl: false // 不显示全屏按钮
	    };
	    
	    var big_url = $(this).next('img').attr('big_url');
	    
	    var img = new Image();
	    
	    img.onload = function(){
	    	
	    	var items = [
		        {
		            src: big_url,
		            w: img.width,
		            h: img.height
		        }
		    ];
		   
		    var gallery = new PhotoSwipe( pswpElement, photoswipeUI, items, options);
			
		    gallery.init();
		    
		    layer.closeAll();
		    
	    }
	    
	    img.src = big_url;
		
	});
	
	
	function loadIndexArea(contId, dataId, url){
			
		var loadMore = require('/widget/moudle/loadMore_index');
		
		var output = new loadMore.output;
		
		output.makeItem = function(data){
			
			//console.log( data.dataType );
		
			if( data.dataType == 'tkk' ) return makeTkkItem(data);
			else if( data.dataType == 'post' ) return makeForumItem(data);
			else if( data.dataType == 'article' ) return makeNewsItem(data);
			else if( data.dataType == 'ad' ) return makeAdItem(data);
		}
		
		output.init(contId, dataId, url);
	}
	
	// 广告数据渲染模板
	function makeAdItem(data){
		
		var t = ['<section class="indexForumItem flexBox">',
				'  <div class="indexFourmL">',
				'    <div class="indexNewsIcon">',
				'      <img src="/template/cis_app/tkk/assets/page/index/images/logo.jpg"  />',
				'    </div>',
				'  </div>',
				'  <div class="indexFourmR indexFourmR-ad flex1">',
				'    <img src="'+ data.photo +'"/>',
				'    <a href="'+ data.url +'" class="ui-moodUserAreaClick"></a>',
				'  </div>',
				'</section>'].join("");
		
		var MI = $(t);
		
		// 针对APP进行修改
		if( tbb.from == 'app' ){
			
			// 取消版块链接：APP无接口
			if( data.url.indexOf('forum')!=-1 && data.url.indexOf('.html')!=-1 ) MI = null;
		}
		
		return MI
	}
	
	// 知识数据渲染模板
	function makeNewsItem(data){
		
		var t = ['<section class="indexForumItem flexBox">',
				'	<div class="indexFourmL">',
				'		<div class="indexNewsIcon">',
				'           <span>知识<br/>推荐</span>',
				'		    <a href="'+ data.channelUrl +'" class="ui-moodUserAreaClick gettab" tab="list_'+ data.channelId +'"></a>',
				'       </div>',
				'	</div>',
				'	<div class="indexFourmR flex1">',
				'		<div class="indexFourmBox flexBox">',
				'			<div class="flex1"><h3>'+ data.title +'</h3></div>',
				'		</div>',
				'		<div class="indexFourmBot">资讯 · '+ data.channel +'</div>',
				'		<a href="'+ data.url +'" class="ui-moodUserAreaClick gettab" tab="view"></a>',
				'	</div>',
				'</section>'].join("");
				
		var MI = $(t);
		
		return MI
	}
	
	// 帖子数据渲染模板
	function makeForumItem(data){
		
		var t = ['<section class="indexForumItem flexBox">',
				'	<div class="indexFourmL">',
				'		<div class="indexFourmIcon">',
				'			<span>'+ CONFIG.getForumSimpleName(data.forumId) +'</span>',
				'		    <a href="'+ data.forumUrl +'" class="ui-moodUserAreaClick gettab" tab="forum_'+ data.forumId +'"></a>',
				'		</div>',
				'	</div>',
				'	<div class="indexFourmR flex1">',
				'		<div class="indexFourmBox flexBox">',
				'			<img src="'+ data.photo +'" alt="" />',
				'			<div class="flex1">',
				'				<h3>'+ data.title +'</h3>',
				'				<p>'+ data.summary +'</p>',
				'			</div>',
				'		</div>',
				'		<div class="indexFourmBot">',
				'            #'+ data.forumRealName +'&nbsp;&nbsp;by '+ data.author,
				'		</div>',
				'		<a href="'+ data.url +'" class="ui-moodUserAreaClick gettab" tab="viewthread"></a>',
				'	</div>',
				'</section>'].join("");
		
		var MI = $(t);
		
		// 如果图片不存在，remove；如果存在，隐藏文字摘要
		if( data.photo == "" || data.photo == null ) MI.find('img').remove();
		else MI.find('.indexFourmBox p').remove();
		
		// 针对APP进行修改
		if( tbb.from == 'app' ){
			
			// 取消版块链接：APP无接口
			MI.find('.indexFourmIcon a').remove();
			
			// 替换帖子打开方式为APP接口
			MI.find('.indexFourmR a').remove();
			MI.find('.indexFourmR').append('<span class="ui-moodUserAreaClick" onclick="sq.topicDetail('+data.topicId+','+data.forumId+',1)"></span>');
		}
		
		return MI
		
	}
	
	// 糖控控数据渲染模板
	function makeTkkItem(data){
		
		var t = ['<section class="indexMoodItem" data-id="'+ data.id +'" data-userId="'+ data.userId +'">',
				'    <div class="ui-moodUserArea flexBox">',
				'        <div class="ui-moodAvatar">',
				'            <a href="'+ data.ucenterURL +'" class="gettab" tab="space"><img src="'+ data.avatar +'" alt="" /></a>',
				'        </div>',
				'        <div class="flex1">',
				'            <span class="ui-moodName"><a href="'+ data.ucenterURL +'" class="gettab" tab="space">'+ data.name +'</a></span>',
				'            <span class="ui-moodInfo"><a href="'+ data.ucenterURL +'" class="gettab" tab="space"><i class="man">&nbsp;</i>'+ data.userInfo +'</a></span>',
				'        </div>',
				'    </div>',
				'    <div class="ui-moodContArea flexBox">',
				'        <div class="ui-moodBV">',
				'            <span class="ui-moodBV-value">'+ data.bloodValue +'</span>',
				'            <span class="ui-moodBV-name">'+ CONFIG.getBdName(data.bloodName) +'</span>',
				'        	<a href="'+ data.statURL +'" tab="page_viewBlood" class="ui-moodUserAreaClick gettab"></a>',
				'        </div>',
				'        <div class="flex1">',
				'            <div class="indexMoodArea1">',
				'                <p class="ui-moodText">',
				'                    '+ data.moodText +'',
				'                </p>',
				'                <ul class="ui-moodRelated">',
				'                    <li class="flexBox">',
				'                        <span class="icon"><i class="food"></i></span>',
				'                        <p class="flex1">'+ data.moodRelate.food +'</p>',
				'                    </li>',
				'                    <li class="flexBox">',
				'                        <span class="icon"><i class="medicine"></i></span>',
				'                        <p class="flex1">'+ data.moodRelate.medicine +'</p>',
				'                    </li>',
				'                    <li class="flexBox">',
				'                        <span class="icon"><i class="sports"></i></span>',
				'                        <p class="flex1">'+ data.moodRelate.sport +'</p>',
				'                    </li>',
				'                </ul>',
				'        		<a href="'+ data.moodURL +'" tab="page_viewMood" class="ui-moodUserAreaClick gettab"></a>',
				'            </div>',
				'            <div class="ui-moodPhoto">',
				'                <a href="javascript:void(0)" class="ui-moodUserAreaClick"></a>',
				'                <img src="" big_url="'+ data.photo.big_url +'" _width="" _height="" alt="" />',
				'            </div>',
				'    		<div class="ui-moodBootArea fn-clear">',
				'        		<div class="fn-left ui-moodTime">'+ DateUtil.difftimeToStr(false, data.date) +'</div>',
				'        		<div class="fn-right">',
				'            		<span class="ui-moodLike" onclick="FUNC.liked(this, '+ data.id +')"><i class="like"></i>赞 <span>'+ data.likeCount +'</span></span>',
				'            		<a href="'+ data.commentURL +'" tab="page_viewComment" class="gettab ui-moodComment ui-moodComment-'+data.id+'"><i class="comment"></i>评论 <span>'+ data.commentCount +'</span></a>',
				'        		</div>',
				'    		</div>',
				'        </div>',
				'    </div>',
				
				'</section>',
				].join("");
		
		var MI = $(t);
		
		// 更改性别图标
		if( data.sex == 2) MI.find('.man').attr("class","woman");
		
		// 判断更改血糖颜色
		var bdArr = CONFIG.getBdRange(data.bloodName),
			$moodBV = MI.find('.ui-moodBV');
			
		if( data.bloodValue < bdArr[1] ) $moodBV.addClass('ui-moodBV3');
		else if( data.bloodValue > bdArr[2] ) $moodBV.addClass('ui-moodBV2');
		
		// 判断显示历史测试日期：如果年、月、日中有1项不相等
		// 因为后台数据返回的是Unix时间戳，单位为秒，所以需要*1000转换为毫秒数
		var dateTest = DateUtil.longToDate(parseInt(data.testDate)*1000),
			dateAdd  = DateUtil.longToDate(parseInt(data.date)*1000);
		
		if( dateTest.getFullYear() != dateAdd.getFullYear() || 
			dateTest.getMonth() != dateAdd.getMonth() || 
			dateTest.getDate() != dateAdd.getDate()
		){
			MI.find('.indexMoodArea1').prepend('<span class="ui-moodTestDate">测试日期：'+ DateUtil.dateToStr(dateTest, "yyyy年MM月dd日") +'</span>');
		}
		
		// 判断隐藏说说文字
		if( data.moodText == "" || data.moodText == null ) MI.find('.ui-moodText').remove();
		
		// 判断隐藏相关信息
		var MI_relate = MI.find('.ui-moodRelated li');
		if( data.moodRelate.food == "" || data.moodRelate.food == null ) MI_relate.eq(0).remove();
		if( data.moodRelate.medicine == "" || data.moodRelate.medicine == null ) MI_relate.eq(1).remove();
		if( data.moodRelate.sport == "" || data.moodRelate.sport == null ) MI_relate.eq(2).remove();
		
		// 判断隐藏图片
		if( data.photo.url == "" || data.photo.url == null ) MI.find('.ui-moodPhoto').remove();
		else{
			
			MI.find('.ui-moodPhoto img').attr('src', data.photo.url);
		}
		
		// 显示点赞和评论状态
		if( data.like == true ) MI.find('.like').addClass('on');
		if( data.comment == true ) MI.find('.comment').addClass('on');
		
		// 根据是否自己、关注情况显示操作按钮
		if( data.userId == IndexUserId ){
			
			MI.prepend('<span class="indexMoodSystem" onclick="FUNC.showBotPop(\'actionSystem\', '+data.id+', this)">&nbsp;</span>');
			
		}else{
			
			if( data.follow == false ) MI.prepend('<span class="indexMoodFollow" onclick="FUNC.followd(this, '+ data.userId +')">关注</span>');
			else MI.prepend('<span class="indexMoodEdit" onclick="FUNC.showBotPop(\'actionEdit\', '+data.userId+')">&nbsp;</span>');
		}
		
		// 如果评论不为空		
		if( data.commentCont && data.commentCont.length > 0 ){
			
			var $CT = $('<div class="indexMoodCome"></div>');
			
			for( var k=0; k<=data.commentCont.length-1; k++){
				
				var cData = data.commentCont[k];
				
				var ctm = ['<div class="indexMood-comItem flexBox">',
						'     <p><a href="">'+ cData.name +'</a>：'+ cData.text +'</p>',
						'     <a href="'+ data.commentURL +'" tab="page_viewComment" class="ui-moodUserAreaClick gettab"></a>',
						' </div>'].join("");
				
				var $CTM = $(ctm);
						
				if( cData.target != "" && cData.target != null ) $CTM.find('p a').after(' 回复 <a href="">'+ cData.target +'</a>');
				
				$CT.append($CTM );
				
			}
			
			MI.find('.ui-moodBootArea').before($CT);

		}
		
		
		// 针对APP进行修改
		if( tbb.from == 'app' ){
			
			var u = ['  <div class="ui-moodAvatar">',
					'      <a href="javascript:void(0)" onclick="sq.userCenter('+data.userId+');"><img src="'+ data.avatar +'" alt="" /></a>',
					'   </div>',
					'   <div class="flex1">',
					'      <span class="ui-moodName"><a href="javascript:void(0)" onclick="sq.userCenter('+data.userId+');">'+ data.name +'</a></span>',
					'      <span class="ui-moodInfo"><a href="javascript:void(0)" onclick="sq.userCenter('+data.userId+');"><i class="man">&nbsp;</i>'+ data.userInfo +'</a></span>',
					'   </div>',].join("");
			
			MI.find('.ui-moodUserArea').empty().append(u);
			
		}
		
		return MI
	}
	
	
	return init
	
});