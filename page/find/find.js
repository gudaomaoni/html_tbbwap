define(['base', 'swiper'], function() {

  // 头图切换
  var nxSwiper = new Swiper('#nxSwiper', {
    iOSEdgeSwipeDetection: true,
    pagination: '.swiper-pagination',
    autoplay: 5000,
    speed: 400,
    loop: true
  });

  // 点击 更多
  var nav = $("#nxNav li");
  nav.last().on('click', function() {

    //提示框
    layer.open({
      content: '更多功能敬请期待..',
      skin: 'msg',
      time: 2 //2秒后自动关闭
    });

    return false

  }).css('opacity', 0.6);

  // 微课堂 @获取数据
  $.getJSON(findClassDataURL, function(json) {

    // swiper wrapper
    var $SW = $("#find_ClassSwiper").find('.swiper-wrapper').empty();

    var data = json.news.items;
    // 每三个元素一次循环
    for(var i = 1; i <= Math.ceil(data.length / 3); i++) {

      var $S = $('<div class="swiper-slide flexBox borderBox"></div>');

      // 这次循环的起始坐标点
      var k = (i - 1) * 3;

      for(var j = 0; j < 3; j++) {

        // 当前元素坐标点
        var l = k + j;

        // 如果当前坐标点数据不存在，退出循环。
        if(l > data.length - 1) {
          
          var t = ['<div class="fd-classItem flex1">',
            '&nbsp;',
            '</div>'
          ].join("");
          
        } else {
          
          var t = ['<div class="fd-classItem flex1">',
            '    <a href="' + data[l].url + '" class="block100_a gettab2" tab="view">',
            '        <img src="' + data[l].pic + '" alt="' + data[l].title + '" />',
            '        <p class="color-66">' + data[l].title + '</p>',
            '    </a>',
            '</div>'
          ].join("");
        }

        $S.append(t);

      }

      // 微课堂 @swiper数据嵌入
      $SW.append($S);

    }

    // 微课堂 @swiper切换
    var classSwiper = new Swiper('#find_ClassSwiper', {
      iOSEdgeSwipeDetection: true,
      pagination: '.swiper-pagination',
      //autoplay: 5000,
      //loop: true,
      speed: 400
    });

  });

  // 糖友故事 @获取数据
  $.getJSON(findStoryDataURL, function(json) {

    // swiper wrapper
    var $SW = $("#find_StorySwiper").find('.swiper-wrapper').empty();

    var data = json.news.items;
    // 每三个元素一次循环
    for(var i = 1; i <= Math.ceil(data.length / 3); i++) {

      var $S = $('<div class="swiper-slide flexBox borderBox"></div>');

      // 这次循环的起始坐标点
      var k = (i - 1) * 3;

      for(var j = 0; j < 3; j++) {

        // 当前元素坐标点
        var l = k + j;

        // 如果当前坐标点数据不存在，退出循环。
        if(l > data.length - 1) {
          
          var t = ['<div class="fd-classItem flex1">',
            '&nbsp;',
            '</div>'
          ].join("");
          
        } else {
          
          var t = ['<div class="fd-classItem flex1">',
            '    <a href="' + data[l].url + '" class="block100_a gettab2" tab="view">',
            '        <img src="' + data[l].pic + '" alt="' + data[l].title + '" />',
            '        <p class="color-66">' + data[l].title + '</p>',
            '    </a>',
            '</div>'
          ].join("");
        }

        $S.append(t);

      }

      // 糖友故事 @swiper数据嵌入
      $SW.append($S);

    }

    // 糖友故事 @swiper切换
    var StorySwiper = new Swiper('#find_StorySwiper', {
      iOSEdgeSwipeDetection: true,
      pagination: '.swiper-pagination',
      //autoplay: 5000,
      //loop: true,
      speed: 400
    });

  });

  // 本周排行榜 @获取数据
  $.getJSON(findRankDataURL, {
    uid: tbb.uid
  }, function(json) {

    // 数据列表：对应前台展示的顺序
    var datas = [json.tkk, json.tkkComments, json.posts, json.postComments];

    // swiper wrapper
    var $SW = $("#fd_rankSwiper").find('.swiper-wrapper').empty();

    // 按照顺序循环插入数据
    for(var i = 0; i <= datas.length - 1; i++) {

      // 当前数据集
      var data = datas[i];

      var $S = $('<div class="swiper-slide flexBox borderBox"></div>'),
        $U = $('<ul class="fd-rankList borderBox"></ul>');

      for(var j = 0; j <= data.length - 1; j++) {

        var $t = $(['<li>',
          '    <span class="one">' + (j + 1) + '</span>',
          '    <span class="two"><a href="' + data[j].homeUrl + '" class="block100_a gettab" tab="space"><img src="' + data[j].avatar + '" />' + data[j].username + '</a></span>',
          '    <span class="three">' + data[j].count + '次</span>',
          '</li>'
        ].join(""));

        // 是否为自己/是否已关注
        if(data[j].uid == tbb.uid)  $t.addClass('self').append('<span class="indexMoodFollow on self">自己</span>');
        else if(data[j].follow) $t.append('<span class="indexMoodFollow on">已关注</span>');
        else $t.append('<span class="indexMoodFollow" onclick="FUNC.followd(this, ' + data[j].uid + ')">关注</span>');

        $U.append($t);

      }

      $S.append($U);

      // 本周排行榜 @swiper数据嵌入
      $SW.append($S);

    }

    // 本周排行榜
    var $rankTabs = $("#fd_rankTab span");

    // 本周排行榜 - 调用Swiper插件
    var rankSwiper = new Swiper('#fd_rankSwiper', {
      initialSlide: 0,
      iOSEdgeSwipeDetection: true,
      pagination: '#fd_rankTab',
      paginationType: 'custom',
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {

        // 更换分页当前样式
        $rankTabs.removeClass('current').eq(current - 1).addClass('current');

      },
      speed: 400,
      spaceBetween: 0,
      autoHeight: true
    });

    // 本周排行榜 - 点击切换
    $rankTabs.on('tap', function() {
      $rankTabs.removeClass('current');
      $(this).addClass('current');

      var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
      rankSwiper.slideTo(CurrentPos);
    });

  });

  // 往期H5专题
  var $h5 = $("#fd_h5_container");
  $h5.css("width", $h5.children().length * 106 + 'px');

});