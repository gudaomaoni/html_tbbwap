/* 
 * 添加血糖记录页面JS
 */

define(['zepto', 'base', 'raphael', 'justgage', 'iscroll4', 'datePicker', 'swiper'], function($) {

  // 记录血糖页面切换效果
  // 调用Swiper插件
  var bloodSwiper = new Swiper('#blooAddSwiper', {
    initialSlide: 0,
    onlyExternal: true,
    iOSEdgeSwipeDetection: true,
    paginationType: 'custom',
    speed: 400,
    spaceBetween: 10
  });

  /* 
   * 血糖仪表盘效果
   */

  // 血糖值判断标准数组
  var bdArr = CONFIG.getBdRange($("#bloodAdd_stage").val());

  // 甜蜜亲友团的Canvas代码

  // 实现血糖仪表指针功能
  // 因为时间原因代码暂未整理
  var planeLoad = [];
  var thePointer = null,
    Rx, Ry;

  var PaintMap = {};
  PaintMap.refresh = function(value) {
    data = 180 / 33.3 * value;
    thePointer.animate({
      "transform": "r" + data + "," + Rx + "," + Ry
    }, 700, ">")
  }

  planeLoad.loading = {
    paint: function(PL) {
      this.lineSet(PL);
      this.pointer(PL);
      this.rotate(PL);
    },
    lineSet: function(PL) {
      //圆心点Rx,Ry坐标; Rb为大圆半径,Rs为小圆半径,Ts为显示数字的圆环半径。
      var Rb = PL.config.width / 2 - 45,
        Rs = PL.config.width / 2 - 41;
      //lines是半圆所画线数量； 半圆角度为180度/线数，得到angle是角度，需要将angle转为正弦、余弦计算的弧度，所以乘以0.017453293(2PI/360)。
      var lines = 40,
        angle = (180 / lines) * 0.017453293;
      //仪表盘显示的初始数字
      var numb = 0;
      //每根线代表数值
      var lineValue = lines / PL.txtMaximum;
      for(var i = 0; i <= lines; i++) {
        //sin为相对圆心的正弦,cos为余弦；
        var sin = Math.sin(angle * i),
          cos = Math.cos(angle * i);
        //Xb,Yb为外点相对于圆形的X,Y坐标
        var Xb = Math.floor(Rb * cos),
          Yb = Math.floor(Rb * sin);
        //Xs,Ys为内点相对于圆形的X,Y坐标
        var Xs = Math.floor(Rs * cos),
          Ys = Math.floor(Rs * sin);
        //Xt,Yt为数字相对于圆形的X,Y坐标
        //根据需求定制线段的颜色
        var colors = null;

        if(i <= bdArr[1]) {
          colors = "#FF0000";
        } else if(i < bdArr[2]) {
          colors = "#22AC38";
        } else {
          colors = "orange";
        }
        //根据需求出两点的X,Y坐标，画线段；
        PL.canvas.path("M" + (Rx - Xb) + "," + (Ry - Yb) + "," + (Rx - Xs) + "," + (Ry - Ys)).attr({
          "stroke": colors
        });
      };
    },
    pointer: function(PL) {
      Ry2 = Ry;
      Rx2 = Rx;
      var data = "M65," + Ry2 + "L" + Rx2 + "," + (Ry2 + 4) + "L" + +Rx2 + "," + (Ry2 - 4) + "z";
      thePointer = PL.canvas.path(data);
      thePointer.attr({
        "fill": "#ccc",
        "stroke": " #ccc",
        "stroke-width": 0

      });
    },
    rotate: function(PL) {
      PL.canvas.circle(Rx, Ry, 6).attr({
        "fill": "#ccc",
        "stroke": "#ccc",
      })
      PaintMap.refresh(PL.originalValue);
    }
  }

  function paintMap(PL) {
    Rx = PL.config.width / 2, Ry = PL.config.height - 32.5;
    planeLoad.loading.paint(PL);
  }

  // 调用方法进行表盘绘制
  var dashBoardTarget = new JustGage({
    id: "dashBoardTarget",
    value: "",
    min: 0,
    max: bdArr[3],
    width: 250,
    height: 150,
    gaugeWidthScale: 0.4,
    customSectors: [{
      color: '#FF0000',
      lo: 0,
      hi: bdArr[1]
    }, {
      color: '#22AC38',
      lo: bdArr[1],
      hi: bdArr[2]
    }, {
      color: 'orange',
      lo: bdArr[2],
      hi: bdArr[3]
    }],
    hideMinMax: true,
    hideValue: true,
    counter: false
  });

  // 指针绘制
  paintMap(dashBoardTarget);

  var $bdValue = $("#bloodAdd_value");

  $bdValue.on('keyup', function() {

    updateDashboardrValue(this);

  }).on('blur', function() {

    var value = parseFloat($(this).val());
    if(value < bdArr[0]) $(this).val(bdArr[0]);
    updateDashboardrValue(this);

  });

  // 扩展点击区域
  $(".glucoseValue").on('click', function() {

    $bdValue.trigger('focus');

  });

  // 改变时间段
  $("#bloodAdd_stage").on('change', function() {

    // 改变血糖判断标准
    bdArr = CONFIG.getBdRange($(this).val());

    console.log(bdArr);

    // 修改表盘参数
    dashBoardTarget.refreshSelectors([{
      color: '#FF0000',
      lo: 0,
      hi: bdArr[1]
    }, {
      color: '#22AC38',
      lo: bdArr[1],
      hi: bdArr[2]
    }, {
      color: 'orange',
      lo: bdArr[2],
      hi: bdArr[3]
    }]);

    // 重绘指针
    planeLoad.loading.lineSet(dashBoardTarget);

    var value = $bdValue.val();

    // 改变血糖数值颜色
    $bdValue.removeClass().addClass(changeGlucoseColor(value));

    // 刷新仪表盘
    dashBoardTarget.refresh(value);

  });

  var updateDashboardrValue = function(object) {

    var value = parseFloat($(object).val());

    // 如果为非法数字
    if(isNaN(value)) {

      value = 0;
      $(object).val("");

    } else if($.type(value) == 'number') {

      // 大于33.3的值进行重置
      if(value > bdArr[3]) $(object).val(value = bdArr[3]);
      else if(value < 0) $(object).val(value = 0);
      else {
        // 转换为字符串类型进行小数点保留2位运算
        value = value.toString();
        if(value.split(".") && value.split(".")[1] && value.split(".")[1].length > 2) {
          $(object).val(value.substring(0, value.indexOf(".") + 3));
          return false;
        }
      }

    }

    value = parseFloat(value);

    $(object).removeClass().addClass(changeGlucoseColor(value));

    // 改变仪表盘进度
    dashBoardTarget.refresh(value);
    // 改变指针方位
    PaintMap.refresh(value);

    event.stopPropagation();
    event.preventDefault();
  }

  //改变血糖数值颜色
  var changeGlucoseColor = function(value) {
    if(value <= bdArr[1]) {
      $bdValue.data('level', 3);
      return "redc";
    } else if(value > bdArr[2]) {
      $bdValue.data('level', 2);
      return "orangec";
    } else {
      $bdValue.data('level', 1);
      return "greenc";
    }
    return "orangec";
  }

  /* 
   * 日期选择效果
   */

  // 载入文件
  require('/widget/utils/dateUtil');

  // 时间输入框绑定datePicker事件
  $("#bloodAdd_date").datePicker({
    beginyear: 2015,
    theme: 'datetime'
  });
  
  initDate();

  // 默认为当前日期
  function initDate() {
    
    var nowDate = new Date();
    $("#bloodAdd_date").val(DateUtil.dateToStr(nowDate, 'yyyy年MM月dd日 HH时mm分'));

    // 根据小时默认显示时间段
    var nowHour = nowDate.getHours(),
      nowStageVal = 2;

    // 凌晨时间段
    if(nowHour >= 2 && nowHour < 6) {
      nowStageVal = 1

      // 空腹时间段
    } else if(nowHour >= 6 && nowHour < 8) {
      nowStageVal = 2

      // 早餐后时间段
    } else if(nowHour >= 8 && nowHour < 10) {
      nowStageVal = 3

      // 午餐前时间段
    } else if(nowHour >= 10 && nowHour < 12) {
      nowStageVal = 4

      // 午餐后时间段
    } else if(nowHour >= 12 && nowHour < 15) {
      nowStageVal = 5

      // 晚餐前时间段
    } else if(nowHour >= 15 && nowHour < 18) {
      nowStageVal = 6

      // 晚餐后时间段
    } else if(nowHour >= 18 && nowHour < 21) {
      nowStageVal = 7

      // 睡前时间段
    } else if(nowHour >= 21 || nowHour < 2) {
      nowStageVal = 8
    }

    $("#bloodAdd_stage").val(nowStageVal);
    $("#bloodAdd_stage").find("option[value='" + nowStageVal + "']").attr("selected", "selected");
    $("#bloodAdd_stage").trigger('change');
    
  };

  // 确定按钮
  $("#bloodAddSubmit").on('click', function() {

    if(PAGE.bloodAddGate()) return false;
    else {

      //询问框
      layer.open({
        content: '请选择您想要进行的操作',
        btn: ['继续发糖控控', '仅保存血糖'],
        // 只保存血糖
        no: function(index) {
          saveBlood();
        },
        // 发糖控控
        yes: function(index) {
          layer.close(index);

          bloodSwiper.slideTo(1);
          $("#bloodAddHead").hide();
          $("#bloodPostHead").show();
          PAGE.bloodPost();
        }
      });

    }

  });

  $("#bloodPostBack").on('click', function() {

    bloodSwiper.slideTo(0);
    $("#bloodPostHead").hide();
    $("#bloodAddHead").show();
  });

  /* 只保存血糖，不发表糖控控 */
  $("#bloodSaveBtn").on('click', function() {

    if(PAGE.bloodAddGate()) return false;

    //询问框
    layer.open({
      content: '您确定只保存血糖值，不发表糖控控？',
      btn: ['确定', '取消'],
      no: function(index) {
        //      bloodSwiper.slideTo(1);
        //      $("#bloodAddHead").hide();
        //      $("#bloodPostHead").show();
        //      PAGE.bloodPost();
        layer.close(index);
      },
      // 只保存血糖
      yes: function(index) {
        saveBlood();
      }
    });
  });

  // 保存血糖数据，不发表糖控控
  function saveBlood() {

    var data = {
      value: $("#bloodAdd_value").val(),
      stage: $("#bloodAdd_stage").val(),
      food: $("#bloodAdd_food").html(),
      drug: $("#bloodAdd_drug").html(),
      sport: $("#bloodAdd_sport").html(),
      date: $("#bloodAdd_date").val(),
      level: $("#bloodAdd_value").data('level')
    };

    console.log($.param(data));

    layer.open({
      type: 2,
      content: '正在保存中'
    });

    $.post(bloodPostURL, $.param(data), function(response) {

      layer.closeAll();

      // 格式化字符串
      var data = eval("(" + response + ")");

      if(data.success == true) {

        layer.open({
          content: '血糖数据保存成功 √',
          btn: ['继续记录', '查看数据'],
          // 继续记录
          yes: function() {
            layer.closeAll();

            // 还原血糖值，仪表盘状态
            $("#bloodAdd_value").val("").trigger('keyup');
            // 更新测量点和测量时间
            initDate();
            // 重置附加描述
            $("#bloodAdd_food").val("");
            $("#bloodAdd_drug").val("");
            $("#bloodAdd_sport").val("");

          },
          no: function() {
            window.location.href = '/tkk.php?mod=statistics&mobile=2';
          }
        });

      } else {

        alert('出错啦！请与工作人员进行联系~');
      }
    })

  }

  /* 
   * 页面滚动
   */
  var pageScroll = require('/widget/moudle/pageScroll');
  new pageScroll("bloodAddCont");

});