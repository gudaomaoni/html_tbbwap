/* 
 * 发布血糖页面JS
 */

define(['base', 'iscroll'], function() {

  /* 
   * 页面滚动
   */

  var heightCanUse = $("#bloodAddCont").height(),
    heightContent = document.getElementById('bloodAddCont').scrollHeight;

  // 如果页面内容超出，设置滚动
  if(heightContent > heightCanUse) {

    $("#bloodPostDataContent").css("overflow", "hidden").height(heightCanUse - 151);

    var bloodPostScroll = new IScroll("#bloodPostDataContent", {
      click: true,
      vScrollbar: false,
      bounce: false,
      bindToWrapper: true // 只有在这个层上触摸才能滑动，避免影响textarea的滑动
    });

  }

  // 图片上传：参数
  var bdPhotoCover = $(".bloodPostPhoto");

  // 图片上传：按钮
  $("#bloodPostPhoto").on('change', function(e) {

    var t = e.target,
      file = t.files[0],
      fileSize = file.size,
      fileType = file.type;

    console.log("源文件：" + fileSize, fileType);

    // 判断图片类型
    if(fileType != 'image/jpeg' && fileType != 'image/png') {

      layer.open({
        content: '请上传jpg、png格式的图片！',
        className: 'layerInfo',
        time: 5
      });
      this.value = "";
      return false;
    }

    // 文件判断
    if(fileSize / 1024 > 5000) {

      layer.open({
        content: '图片尺寸不能超过5M，请重新上传！',
        className: 'layerInfo',
        time: 5
      });
      this.value = "";
      return false;
    }

    // 取消之前的图片预览
    bdPhotoCover.attr("style", "");

    // 显示上传状态
    bdPhotoCover.addClass('load');

    var reader = new FileReader();

    // 文件读取完成之后
    reader.onload = function() {

      // 通过 reader.result 来访问生成的 DataURL
      var DataURL = reader.result;

      // 创建表单数据，添加新的data数据
      var vFD = new FormData();

      // 如果是JPG格式，压缩处理成Blob对象
      if(fileType == 'image/jpeg') {

        // 新建IMG对象，准备读取源文件进去
        var img = new Image();

        img.onload = function() {

          var blobx = getJpegBlob(img);

          if(blobx.size > 0) vFD.append("file", blobx);
          else vFD.append("file", file);

          // 上传图片
          uploadPhoto(vFD, DataURL);

          console.log("转换后blob：" + blobx.size, blobx.type);
        }

        img.src = DataURL;

      } else {

        vFD.append("file", file);
        // 上传图片
        uploadPhoto(vFD, DataURL);
      }

    };

    // 本地读取文件
    reader.readAsDataURL(file);

  });

  /**** 图片上传函数 ****/
  function uploadPhoto(vFD, DataURL) {

    // 创建文件上传对象
    var oXHR = new XMLHttpRequest();
    oXHR.onreadystatechange = function() {

      // 4 = "loaded"
      if(this.readyState == 4) {

        console.log(this.responseText);

        // 隐藏上传状态
        bdPhotoCover.removeClass('load');

        // 文件上传成功事件					
        if(this.status == 200) {

          // 格式化字符串
          var data = eval("(" + this.responseText + ")");

          if(data.state == 200) {

            // 显示图片预览
            bdPhotoCover.css("background-image", "url(" + DataURL + ")");

            $("#bloodPostPhotoURL").val(data.url);

          } else {

            alert(this.responseText)
          }

          // 文件上传的错误事件
        } else {

          layer.open({
            content: '抱歉，图片上传失败，请重新上传！',
            className: 'layerInfo',
            time: 4
          });
        }
      }
    };
    oXHR.open('POST', photoPostURL);
    //oXHR.setRequestHeader('Content-Type', 'mulipart/form-data');
    oXHR.send(vFD);

  }

  /**** 等比例压缩JPG图片 ****/
  function getJpegBlob(img) {

    var ow = img.width,
      oh = img.height;

    // 如果图片宽度大于720，宽度设为720，高度等比例缩小
    if(ow > 720) {

      oh = oh * (720 / ow);
      ow = 720;
    }

    // 如果高度大于1280，高度设为1280，宽度等比例缩小
    if(oh > 1280) {

      ow = ow * (1280 / oh);
      oh = 1280;
    }

    // 调用canvas重绘图片
    var canvas = $('<canvas width="' + ow + '" height="' + oh + '"></canvas>')[0];
    var ctx = canvas.getContext("2d");

    ctx.drawImage(img, 0, 0, ow, oh);

    // 转换为blob对象
    var blob = convertCanvasToBlob(canvas);

    return blob
  }

  /**** base64转blob ****/
  // 不支持android 2及以下，ios 5.1及以下版本的浏览器
  function convertCanvasToBlob(e) {
    var t, i, s, n, r, a, o, c;
    for(n = "image/jpeg", t = e.toDataURL(n), i = window.atob(t.split(",")[1]), r = new window.ArrayBuffer(i.length), a = new window.Uint8Array(r), s = 0; s < i.length; s++) a[s] = i.charCodeAt(s);
    return o = window.WebKitBlobBuilder || window.MozBlobBuilder, o ? (c = new o, c.append(r), c.getBlob(n)) : new window.Blob([r], {
      type: n
    })
  }

  // 发布数据
  $("#bloodPostSubmit").on('click', function() {

    // 判断是否有说说文字
    if($("#bloodPostText").val() == "") {
      layer.open({
        content: '请先输入说说文字.',
        className: 'layerInfo',
        end: function() {
          $("#bloodPostText").focus();
        }
      });
      return false;
    }

    // 判断图片是否上传完成
    if(bdPhotoCover.hasClass('load')) {
      layer.open({
        content: '请稍候，图片正在上传中...',
        className: 'layerInfo',
        time: 4
      });
      return false;
    }

    // 如果正在发布
    if($(this).data('posting') == true) {
      layer.open({
        content: '正在努力发布中...',
        className: 'layerInfo',
        time: 2
      });
      return false;
    }

    // 标记正在发布
    $(this).data('posting', true);

    var bdValue = $("#bloodPostValue").html().toString();

    var vArr = bdValue.split('.');

    // 如果有小数点
    if(vArr.length > 1) {

      var xs = vArr[1].toString();
      xs = xs.substr(0, 1);

      bdValue = vArr[0].toString() + '.' + xs;
    }

    var data = {
      value: parseFloat(bdValue),
      stage: $("#bloodAdd_stage").val(),
      food: $("#bloodPostFood").html(),
      drug: $("#bloodPostDrug").html(),
      sport: $("#bloodPostSport").html(),
      text: $("#bloodPostText").val(),
      photo: $("#bloodPostPhotoURL").val(),
      date: $("#bloodAdd_date").val(),
      level: $("#bloodAdd_value").data('level')
    };

    console.log($.param(data));

    layer.open({
      type: 2
    });

    $.post(bloodPostURL, $.param(data), function(response) {

      $("#bloodPostSubmit").data('posting', false);

      layer.closeAll();

      // 格式化字符串
      var data = eval("(" + response + ")");

      if(data.success == true) {

        layer.open({
          content: '血糖记录发布成功 √',
          className: 'layerSuccess',
          time: 2,
          end: function() {
            window.location.href = CONFIG.SiteURL;
          }
        });

        // 发布按钮解绑发布事件
        $("#bloodPostSubmit").unbind('click').on('click', function() {

          layer.open({
            content: '本次血糖已成功发布，正在跳转回糖控控首页...',
            className: 'layerSuccess',
            shadeClose: false,
            time: 1,
            end: function() {
              window.location.href = CONFIG.SiteURL;
            }
          });

        });

      } else {

        alert('出错啦！请与工作人员进行联系~');
      }
    })

  });

});