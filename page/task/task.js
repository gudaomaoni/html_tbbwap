define(['base', 'swiper'], function() {

  var init = function() {

    // 页面滚动
    var pageScroll = require('/widget/moudle/pageScroll');
    var taskScroll = new pageScroll("taskCont", {
      bounce: true
    });

    // fastclick点击
    //var FastClick = require('fastclick');
    //FastClick.attach($("#page_task").get(0));

    var taskMineArrow = $("#task_mine"),
      taskTab = $("#task_index_tab li"),
      taskMineMenu = $('.task-menu');

    // 任务列表、我的任务切换
    var taskIndexSwiper = new Swiper('#task_index_swiper', {
      iOSEdgeSwipeDetection: true,
      onlyExternal: true,
      pagination: '#task_index_tab',
      paginationType: 'custom',
      hashnav: true,
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {
        // 更换分页当前样式
        taskTab.removeClass('on').eq(current - 1).addClass('on');
      },
      onSlideChangeEnd: function(swiper) {
        // 刷新Iscroll高度
        taskScroll.refresh();
      },
      speed: 400,
      autoHeight: true //高度随内容变化
    });

    // TAB切换点击
    taskTab.on('click', function() {
      var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;

      // 点击我的任务，并且当前处于我的任务
      if(CurrentPos == 1 && $(this).hasClass('on')) {
        taskMineMenu.toggle()
      } else {
        taskMineMenu.hide()
      }

      taskIndexSwiper.slideTo(CurrentPos);
    });

    // 我的任务子菜单点击
    $(document).on('click', '.task-menu', function() {
      taskMineMenu.hide()
    });

    // 我的任务子菜单各项点击
    $(document).on('click', '.task-menu p', function() {
      var index = parseInt($(this).attr("index"));

      // 展示对应的选择分类
      $("#task_mine_data").find('.ti-taskItem').hide();

      if(index == 0) $("#task_mine_data").find('.ti-taskItem').show();
      else $("#task_mine_data").find('.ti-taskItem[cindex="' + index + '"]').show();

      $("#task_mine_text").html($(this).attr("text"));

      // 更新Swiper高度
      taskIndexSwiper.update();

      // 刷新Iscroll高度
      taskScroll.refresh();
    });

    // 数据加载提示
    layer.open({
      className: 'layerLoading',
      content: '任务数据加载中...'
    });

    // 任务大厅数据调用
    $.getJSON(taskDataURL.list, {
      uid: tbb.uid
    }, function(json) {

      var $LS = $('');

      // 如果没有数据
      if(json.items.length == 0) {

        var t = ['<div class="ti-taskItem ti-taskItem-bc0 flexBox">',
          '    <div class="ti-taskText ti-taskNull flex1">',
          '        <p>暂无任务可领取，敬请期待...</p>',
          '    </div>',
          '</div>'
        ].join("");

        $LS = $(t);

      } else {

        var items = json.items;
        for(var i = 0; i <= items.length - 1; i++) {

          var t = ['<div class="ti-taskItem ti-taskItem-bc0 flexBox">',
            '    <div class="ti-taskPerson cf tc">',
            '        <h3>' + items[i].participants_count + '</h3><p>人参与</p>',
            '    </div>',
            '    <div class="ti-taskText flex1">',
            '        <h2>' + items[i].name + '</h2>',
            '        <p><a href="#">点击查看任务详情 →</a></p>',
            '    </div>',
            '    <a class="ti-taskItem-link" onclick="window.location.href=\'' + taskDetailLink + '&id=' + items[i].id + '\'" tab="page_tasking">&nbsp;</a>',
            '    <a href="javascript:void(0)" class="ti-taskBtn tc cf" itemId=' + items[i].id + '>待领取</a>',
            '</div>'
          ].join("");

          var $t = $(t);

          if(items[i].status != 1) $t.find('.ti-taskBtn').addClass('finished').html('已结束');
          // 如果任务已过期
          else if( json._meta.serverTimestamp > items[i].request_end_datetime ) $t.find('.ti-taskBtn').addClass('outed').html('已过期');
          // 如果任务已领取
          else if(items[i].request) $t.find('.ti-taskBtn').addClass('disable').html('已领取');
          
          // 如果未达到领取条件
          if( !items[i].conditions_passed ) $t.addClass('ti-taskItem-disable');

          $LS.after($t);

        }

      }

      $("#task_list_data").append($LS);

      // 关闭加载提示
      layer.closeAll();

      // 更新Swiper高度
      taskIndexSwiper.update();

      // 刷新Iscroll高度
      taskScroll.refresh();

      // 领取任务按钮
      /*$(".ti-taskBtn").on('click', function() {
        if($(this).hasClass('disable')) {
          layer.open({
            content: '您已经领取过该任务了！',
            time: 2 //2秒后自动关闭
          });
          return false;
        }

        if($(this).hasClass('finished')) {
          layer.open({
            content: '抱歉，此任务已经结束..',
            time: 2 //2秒后自动关闭
          });
          return false;
        }
        
        if($(this).hasClass('outed')) {
          layer.open({
            content: '抱歉，此任务已过期..',
            time: 2 //2秒后自动关闭
          });
          return false;
        }

        var _this = this;
        $.ajax({
          type: "POST",
          url: "/api/dz/index.php/task-center/tasks/" + $(_this).attr("itemId") + "/request",
          data: {
            uid: tbb.uid
          },
          dataType: 'json',
          success: function(data) {

            if(data.success) {
              layer.open({
                content: '任务领取成功 √',
                className: 'layerSuccess',
                time: 2
              });
              $(_this).addClass('disable').html('已领取');
              
              // 任务参与人数+1
              var $p = $(_this).parent().find('.ti-taskPerson h3');
              $p.html( parseInt($p.html()) + 1 );

              // 添加至我的任务列表中
              var $t = makeMyTaskHTML(data.data);
              $("#task_mine_data").prepend($t);

              // 移除我的任务里面的空提示
              if($("#tm_task_null").length > 0) $("#tm_task_null").remove();

            } else {
              layer.open({
                content: '出错啦！请与工作人员进行联系~',
                className: 'layerInfo',
                time: 4
              });
            }
          },
          error: function(xhr, type) {
            alert('未知错误，请联系工作人员！')
          }
        });
      });*/

    });

    // 我的任务数据调用
    function getMyTaskData() {

      $.getJSON(taskDataURL.mine, {
        uid: tbb.uid
      }, function(json) {

        var $LS = $('');

        // 如果没有数据
        if(json.items.length == 0) {

          var t = ['<div class="ti-taskItem ti-taskItem-bc0 flexBox" id="tm_task_null">',
            '    <div class="ti-taskText ti-taskNull flex1">',
            '        <p>您还没有领取过任务哦，快去大厅领取吧！</p>',
            '    </div>',
            '</div>'
          ].join("");

          $LS = $(t);

        } else {

          var items = json.items;
          for(var i = 0; i <= items.length - 1; i++) {

            var $t = makeMyTaskHTML(items[i]);

            $LS.after($t);

          }

        }

        $("#task_mine_data").append($LS);

        // 更新Swiper高度
        taskIndexSwiper.update();

        // 刷新Iscroll高度
        taskScroll.refresh();

      });

    }

    getMyTaskData();

    // FUNCTION
    // 根据我的任务单个任务数据返回单个任务HTML
    function makeMyTaskHTML(item) {

      var t = ['<div class="ti-taskItem flexBox">',
        '    <div class="ti-taskPerson cf tc">',
        '        <h3>' + item.task.participants_count + '</h3><p>人参与</p>',
        '    </div>',
        '    <div class="ti-taskText flex1">',
        '        <h2>' + item.task.name + '</h2>',
        '        <p><a href="#">点击查看任务详情 →</a></p>',
        '    </div>',
        '    <a class="ti-taskItem-link" onclick="window.location.href=\'' + taskDetailLink + '&id=' + item.task_id + '&requestId=' + item.id + '\'" tab="page_tasking">&nbsp;</a>',
        '    <a href="javascript:void(0)" class="ti-taskStatus tc"></a>',
        '</div>'
      ].join("");

      var $t = $(t);

      // 审核中
      if(item.audit_status == 0) $t.addClass('ti-taskItem-bcd').attr("cindex", 1).find('.ti-taskStatus').html('审核中');
      // 审核未通过
      else if(item.audit_status == 2) $t.addClass('ti-taskItem-bcd').attr("cindex", 2).find('.ti-taskStatus').html('审核失败');
      // 待开始
      else if(item.status == 0) $t.addClass('ti-taskItem-bca').attr("cindex", 3).find('.ti-taskStatus').html('待开始');
      // 进行中
      else if(item.status == 1) $t.addClass('ti-taskItem-bcb').attr("cindex", 4).find('.ti-taskStatus').html('进行中');
      // 已完成
      else if(item.status == 2) $t.addClass('ti-taskItem-bcc').attr("cindex", 5).find('.ti-taskStatus').html('已完成');
      // 未完成
      else if(item.status == 3 || item.status == 4 || item.status == 10) $t.addClass('ti-taskItem-bcd').attr("cindex", 6).find('.ti-taskStatus').html('未完成');

      return $t;

    }

  }

  return init;

});