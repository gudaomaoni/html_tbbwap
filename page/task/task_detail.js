define(['base', 'swiper'], function() {
  
	// 任务列表、我的任务切换
  var taskDetailSwiper = new Swiper('#task_detail_swiper', {
    iOSEdgeSwipeDetection: true,
    pagination: '#task_detail_tab',
    paginationType: 'custom',
    /* 自定义模式 */
    paginationCustomRender: function(swiper, current, total) {

      // 更换分页当前样式
      $("#task_detail_tab li").removeClass('on').eq(current - 1).addClass('on');

    },
    speed: 400,
    autoHeight: true //高度随内容变化
  });
  
  $("#task_detail_tab li").on('click', function(){
    var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
    taskDetailSwiper.slideTo(CurrentPos);
  });
  
  // fastclick点击
  var FastClick = require('fastclick');
  FastClick.attach($("#page_detail").get(0));
  
});