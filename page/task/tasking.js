define(['base', 'swiper'], function() {

  var init = function() {
    
    // 页面滚动
    var pageScroll = require('/widget/moudle/pageScroll');
    var taskingScroll = new pageScroll("taskingCont", {
      bounce: true
    });

    // 任务列表、我的任务切换
    var taskDetailSwiper = new Swiper('#tasking_swiper', {
      iOSEdgeSwipeDetection: true,
      pagination: '#tasking_tab',
      paginationType: 'custom',
      onlyExternal: true,
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {

        // 更换分页当前样式
        $("#tasking_tab li").removeClass('on').eq(current - 1).addClass('on');

      },
      onSlideChangeEnd: function(swiper) {
        // 刷新Iscroll高度
        taskingScroll.refresh();
      },
      speed: 400,
      autoHeight: true //高度随内容变化
    });

    $("#tasking_tab li").on('click', function() {
      var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
      taskDetailSwiper.slideTo(CurrentPos);
    });

    // 数据加载提示
    layer.open({
      className: 'layerLoading',
      content: '数据加载中...'
    });

    // 获取数据
    $.getJSON(taskingDataURL, function(data) {

      var actions = data.actions, // 任务内容
        actors = data.participationUsers; // 参与糖友

      // 任务共用数据
      var ndata = {
        name: data.name, // 任务名称
        text: data.description, // 任务描述
        rDateBegin: data.request_begin_datetime ? DateUtil.dateToStr(DateUtil.longToDate(data.request_begin_datetime * 1000), "yyyy-MM-dd") + " 00:00" : null, // 申领开始时间
        rDateEnd: data.request_end_datetime ? DateUtil.dateToStr(DateUtil.longToDate(data.request_end_datetime * 1000), "yyyy-MM-dd") + " 23:59" : null, // 申领结束时间
        dateBegin: data.begin_datetime ? DateUtil.dateToStr(DateUtil.longToDate(data.begin_datetime * 1000), "yyyy-MM-dd") + " 00:00" : null, // 任务开始时间
        dateEnd: data.end_datetime ? DateUtil.dateToStr(DateUtil.longToDate(data.end_datetime * 1000), "yyyy-MM-dd") + " 23:59" : null, // 任务结束时间
        allowUsers: data.max_users > 0 ? data.min_users + '~' + data.max_users + '人' : data.min_users + '人以上', // 最少参与人数
        actors: data.participants_count // 目前参与人数
      };

      // 完成限时
      ndata.allowDays = (data.days > 0 ? data.days + '天' : ndata.dateEnd + '之前');

      // 个人私有数据
      var mdata = data._meta.request;

      // 获取任务状态
      var status, statusName = ["已取消", "已结束", "等待开领", "已过期", "待领取", "审核中", "审核未通过", "待开始", "进行中", "已完成", "未完成"];
      // [0] 已取消
      if(data.status == 3 || mdata.status == 10) status = 0;
      // [1] 已结束
      else if(data.status == 4 || data.status == 5 || data.status == 2) status = 1;
      // [2] 等待开始领取(当前时间 < 申领开始时间)
      else if(data._meta.serverTimestamp < data.request_begin_datetime) status = 2;
      // [3] 已过期(当前时间 > 申领结束时间 && 未领取)
      else if(data._meta.serverTimestamp > data.request_end_datetime && !data._meta.requested) status = 3;
      // [4] 待领取(尚未领取)
      else if(!data._meta.requested) status = 4;
      // [5] 审核中
      else if(mdata.audit_status == 0) status = 5;
      // [6] 审核未通过
      else if(mdata.audit_status == 2) status = 6;
      // [7] 待开始(已领取，审核通过)
      else if(mdata.status === 0) status = 7;
      // [8] 进行中
      else if(mdata.status === 1) status = 8;
      // [9] 已完成
      else if(mdata.status === 2) status = 9;
      // [10] 未完成
      else if(mdata.status === 3 || mdata.status === 4) status = 10;

      // 渲染主要数据
      var MC = $("#tasking_data_main");

      // 任务名称
      MC.append('<h2>' + data.name + '<span class="status' + status + '">（' + statusName[status] + '）</span></h2>');

      // 重要数据展示，不同状态重要数据也不一样
      // 已取消/已结束：显示备注
      if(status == 0 || status == 1) {
        if( mdata.status == 10 ) MC.append('<h3><span>提示信息：</span><b>' + mdata.remark + '</b></h3>');
        else MC.append('<h3><span>提示信息：</span><b>' + data.remark + '</b></h3>');
        // 等待开始领取
      } else if(status == 2) {
        MC.append('<h3><span>申领开始：</span><b class="blue">' + ndata.rDateBegin + '</b></h3>');
        if(data.conditions) MC.append('<h3><span>领取条件：</span>' + data.conditions + '</h3>');
        // 已过期
      } else if(status == 3) {
        if(ndata.rDateEnd) MC.append('<h3><span>申领结束：</span><b>' + ndata.rDateEnd + '</b></h3>');
        // 待领取
      } else if(status == 4) {
        if(ndata.rDateEnd) MC.append('<h3><span>申领结束：</span><b>' + ndata.rDateEnd + '</b></h3>');
        if(data.conditions) MC.append('<h3><span>领取条件：</span>' + data.conditions + '</h3>');
        MC.append('<h3><span>完成限时：</span>' + ndata.allowDays + '</h3>');
        // 审核中
      } else if(status == 5) {
        MC.append('<h3><span>提示信息：</span>正在审核中，请耐心等候..</h3>');
        if(data.max_users > 0) {
          MC.append('<h3><span>目前参与人数：</span><b>' + ndata.actors + '</b></h3>');
          MC.append('<h3><span>要求参与人数：</span>' + ndata.allowUsers + '</h3>');
        }
        // 审核未通过
      } else if(status == 6) {
        MC.append('<h3><span>提示信息：</span><b>' + mdata.remark + '</b></h3>');
        if(data.max_users > 0) {
          MC.append('<h3><span>目前参与人数：</span><b>' + ndata.actors + '</b></h3>');
          MC.append('<h3><span>要求参与人数：</span>' + ndata.allowUsers + '</h3>');
        }
        // 待开始
      } else if(status == 7) {
        MC.append('<h3><span>任务开始：</span><b>' + ndata.dateBegin + '</b></h3>');
        MC.append('<h3><span>完成限时：</span>' + ndata.allowDays + '</h3>');
        // 进行中
      } else if(status == 8) {
        var endtime = (data.days > 0 ? mdata.start_datetime + 86400 * data.days : data.end_datetime);
        MC.append('<h3><span>剩余时间：</span><b>' + DateUtil.difftimeToStr2(data._meta.serverTimestamp, endtime) + '</b></h3>');
        // 已完成
      } else if(status == 9) {
        if(mdata.finish_datetime) MC.append('<h3><span>完成时间：</span><b class="green">' + DateUtil.dateToStr(DateUtil.longToDate(mdata.finish_datetime * 1000)) + '</b></h3>');
        // 未完成
      } else if(status == 10) {
        MC.append('<h3><span>结束时间：</span><b class="gray">' + DateUtil.dateToStr(DateUtil.longToDate(mdata.expired_datetime * 1000)) + '</b></h3>');
      }

      // 任务奖励
      MC.append('<h3><span>完成奖励：</span>' + data.awards + '</h3>');

      // 任务描述
      var $des = $('<p><span>任务描述：</span>' + data.description + '</p>');
      MC.append($des);

      /* 任务描述展开效果 */
      var desMaxH = 65,
        desH = $des.height();

      if(desH > desMaxH) {
        //任务详情显示隐藏
        $des.css('max-height', '65px');
        $(".tasking-detail-open").show().on('click', function() {
          $(".tasking-detail p").toggleClass('tasking-detail-ov');
          $(".tasking-detail-open h4 i").toggleClass('imui_icon_off');
        });
      }

      // 渲染任务内容
      for(var i = 0; i <= actions.length - 1; i++) {
        var $t = $(['<p class="flexBox">',
          '    <span class="flex1">' + actions[i].name + '</span>',
          '    <span class="flex1 tc">' + actions[i].finish_quantity + '</span>',
          '    <span class="flex1 tc">' + actions[i].actual_finish_quantity + '/' + actions[i].finish_quantity + '</span>',
          '    <span class="flex1 tr"><a onclick="window.location.href=\'' + actions[i].url + '\'">前往</a></span>',
          '</p>'
        ].join(""));

        // 如果某项已完成
        if(actions[i].actual_finish_quantity >= actions[i].finish_quantity) $t.find('span:last-child a').addClass('ready').attr("href", "javascript:void(0)").html('已完成');

        $("#tasking_data_actions").append($t);

      }

      // 渲染详细参数
      var details = [];
      details.push({
        "options": "任务名称",
        "val": ndata.name
      });
      details.push({
        "options": "完成奖励",
        "val": data.awards ? data.awards : '无'
      });
      details.push({
        "options": "领取条件",
        "val": data.conditions ? data.conditions : '无'
      });
      details.push({
        "options": "完成限时",
        "val": ndata.allowDays
      });
      details.push({
        "options": "申领开始",
        "val": (ndata.rDateBegin ? ndata.rDateBegin : "无")
      });
      details.push({
        "options": "申领结束",
        "val": (ndata.rDateEnd ? ndata.rDateEnd : "无")
      });
      details.push({
        "options": "最少参与",
        "val": data.min_users + '人'
      });
      details.push({
        "options": "最多参与",
        "val": data.max_users ? data.max_users + '人' : '不限'
      });
      details.push({
        "options": "任务开始",
        "val": (ndata.dateBegin ? ndata.dateBegin : "无")
      });
      details.push({
        "options": "任务截止",
        "val": (ndata.dateEnd ? ndata.dateEnd : "无")
      });

      for(var i = 0; i <= details.length - 1; i++) {

        var $t = $('<p><span>' + details[i].options + '：</span>' + details[i].val +
          '</p>');

        $("#tasking_data_details").append($t)
      }

      // 渲染参与糖友
      for(var i = 0; i <= actors.length - 1; i++) {

        // 已完成
        if(actors[i].finished) var $t = $('<li><i></i><img src="' + actors[i].avatar + '" alt="">' + actors[i].username + '</li>');
        else var $t = $('<li class="task_unjoin"><i></i><img src="' + actors[i].avatar + '" alt="">' + actors[i].username + '</li>');

        $("#tasking_data_requests").append($t);
      }
      
      // 如果任务状态处于非进行中，将任务前往按钮改为透明0.6
      if( status != 8 ){
        $("#tasking_data_actions").addClass('disable');
      }

      // 待领取：显示底部领取任务按钮
      if( status == 4 ) {
        $("#page_tasking").removeClass('contarea');
        $("#tasking_btn").css('display', 'block').on('click', function() {
          $.ajax({
            type: "POST",
            url: taskingApplyURL,
            data: {
              uid: tbb.uid
            },
            dataType: 'json',
            success: function(data) {
              if(data.success) {
                layer.open({
                  content: '任务领取成功 √',
                  className: 'layerSuccess',
                  time: 2,
                  end: function() {
                    //window.location.reload();
                    window.location.href = '/taskCenter.php?mod=index#slide2'
                  }
                });
              } else {
                layer.open({
                  content: data.error.message,
                  className: 'layerInfo',
                  time: 8
                });
              }
            },
            error: function(xhr, type) {
              alert('未知错误，请联系工作人员！')
            }
          });
        });
      }

      // 关闭加载提示
      layer.closeAll();

      // 更新Swiper高度
      taskDetailSwiper.update();
      
      // 刷新Iscroll高度
      taskingScroll.refresh();

    });

    // fastclick点击
    //var FastClick = require('fastclick');
    //FastClick.attach($("#page_tasking").get(0));

  }
  
  return init;

});