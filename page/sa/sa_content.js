define(['base'], function() {
  
  var init = function(){
  
    // fastclick点击
    var FastClick = require('fastclick');
    FastClick.attach($(".sac-shareBtn").get(0));
    
    $(".sac-shareBtn").on('click', function(){
      $(".sac-share-bg").show();
      $(".sac-share").show().one('click', function(){
        $(".sac-share-bg").hide();
        $(this).hide();
      });
  
    });
  
  }
  
  return init;
  
});