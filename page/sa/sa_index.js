define(['base', 'swiper'], function() {

  var init = function() {

    // 任务列表、我的任务切换
    var saSwiper = new Swiper('#sa_swiper', {
      iOSEdgeSwipeDetection: true,
      pagination: '#sa_tab',
      paginationType: 'custom',
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {

        // 更换分页当前样式
        $("#sa_tab li").removeClass('on').eq(current - 1).addClass('on');

      },
      speed: 400,
      autoHeight: true //高度随内容变化
    });

    $("#sa_tab li").on('click', function() {
      var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
      saSwiper.slideTo(CurrentPos);
    });

    // fastclick点击
    var FastClick = require('fastclick');
    FastClick.attach($("#page_sa_index").get(0));
    
  }
  
  return init;
  
});