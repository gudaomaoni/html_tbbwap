define(['base', 'swiper', 'zepto'], function() {

  var init = function() {

    // 数据加载提示
    layer.open({
      className: 'layerLoading',
      content: '数据加载中...'
    });

    // 会员数据调用
    $.getJSON(vipWelfareDURL, {
      uid: tbb.uid
    }, function(json) {

      var data = json.data;
      $("#vip_wf_avatar").attr("src", data.avatar);

      var $vipInfo = $("#vip_wf_name");

      // 会员等级显示
      if(data.medal_type == 0) $vipInfo.html(data.username + '（大众会员 ）');
      else if(data.medal_type == 1) $vipInfo.html(data.username + '（金牌会员 ）');
      else if(data.medal_type == 2) $vipInfo.html(data.username + '（银牌会员 ）');
      else if(data.medal_type == 3) $vipInfo.html(data.username + '（铜牌会员 ）');

      // 关闭加载提示
      layer.closeAll();

    });

    // 会员等级
    var medal = typeof(tbb.medal) != 'undefined' ? (tbb.medal.type ? tbb.medal.type : 0) : 0;
    
    // 转化为页面元素顺序
    var medalP = 0;
    if( medal == 1 ) medalP = 3;
    else if( medal == 3 ) medalP = 1;
    else if( medal == 2 ) medalP = 2;

    // 当前等级特权突出显示
    $(".vip-welfare-cont2").eq(medalP).addClass('vip-current');

    // 任务列表、我的任务切换
    var taskIndexSwiper = new Swiper('#vip_welfare_swiper', {
      initialSlide: medalP,
      iOSEdgeSwipeDetection: true,
      pagination: '#vip_welfare_tab',
      paginationType: 'custom',
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {

        // 更换分页当前样式
        $("#vip_welfare_tab li").removeClass('on').eq(current - 1).addClass('on');

      },
      speed: 400,
      autoHeight: true //高度随内容变化
    });

    $("#vip_welfare_tab li").on('click', function() {
      var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
      taskIndexSwiper.slideTo(CurrentPos);
    });

    // fastclick点击
    var FastClick = require('fastclick');
    FastClick.attach($("#page_vip_welfare").get(0));

  }
  
  return init;

});