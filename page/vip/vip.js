define(['base', 'swiper'], function() {

  var init = function() {

    var $vipInfo = $("#vip-info");

    // 数据加载提示
    layer.open({
      className: 'layerLoading',
      content: '数据加载中...'
    });

    // 会员数据调用
    $.getJSON(vipDataURL.user, {
      uid: tbb.uid
    }, function(json) {
      if(!json.success) {
        $vipInfo.append('<p style="padding-left:16px">数据获取失败，请 <a href="javascript:window.location.reload();" style="text-decoration:underline">点此刷新</a>。</p>')
        return false;
      }

      var data = json.data;
      $vipInfo.append('<img src="' + data.avatar + '" />');
      $vipInfo.append('<h3>' + data.username + '</h3>');

      // 会员等级显示
      if(data.medal_type == 0) $vipInfo.append('<p>会员等级：<span>大众会员</span></p>');
      else if(data.medal_type == 1) $vipInfo.append('<p>会员等级：<span class="orange">金牌会员</span></p>');
      else if(data.medal_type == 2) $vipInfo.append('<p>会员等级：<span class="orange">银牌会员</span></p>');
      else if(data.medal_type == 3) $vipInfo.append('<p>会员等级：<span class="orange">铜牌会员</span></p>');

      // 剩余天数展示
      if(data.medal_type != 0) $vipInfo.append('<p>剩余天数：<span class="orange">' + data.medal_remaining_days + '</span></p>');

      // 会员福利展示
      $("#vip-welfare-" + data.medal_type).show();

    });

    var $dayTask = $("#vip-day-list");

    // 每日任务数据调用
    $.getJSON(vipDataURL.daylist, {
      uid: tbb.uid
    }, function(json) {
      
      // 关闭加载提示
      layer.closeAll();

      // 如果为空对象，代表大众会员
      if(typeof(json.data) == 'undefined' || typeof(json.data.signin) == 'undefined') {
        $dayTask.find('h2').html("每日任务：暂无");
        $(".vip-task-info").html("<a href='http://bbs.tangbangbang.cn/taskCenter.php?mod=index'>只有铜牌、银牌、金牌会员才有每日任务，快去任务中心领取会员任务吧。 <span>点击前往 →</span></a>");
        return;
      }

      var data = json.data;

      // 日常任务都完成了
      if(data.finished) $dayTask.find('h2>span').addClass('ready').html("已完成");
      else $dayTask.find('h2>span').html("进行中");

      // 插入任务表头
      $dayTask.append('<h3 class="flexBox tc-45"><span class="flex1">名称</span><span class="flex1 tc">目标</span><span class="flex1 tc">完成度</span><span class="flex1 tc">状态</span></h3>');

      // 显示签到数据
      if(data.plan_signin) {
        var $t_signin = $(['<p class="flexBox">',
          '    <span class="flex1">签到</span>',
          '    <span class="flex1 tc">1次</span>',
          '    <span class="flex1 tc">'+ (data.signin ? "1" : "0") +'/1</span>',
          '    <span class="flex1 tc"><a href="'+ data.signin_url +'">前往</a></span>',
          '</p>'
        ].join(""));

        if(data.signin) $t_signin.find('span:last-child').addClass('ready').find('a').html("已完成");

        $dayTask.append($t_signin);
      }

      // 显示记录血糖数据
      if(data.plan_tkk > 0) {
        var $t_tkk = $(['<p class="flexBox">',
          '    <span class="flex1">记录血糖</span>',
          '    <span class="flex1 tc">' + data.plan_tkk + '次</span>',
          '    <span class="flex1 tc">' + data.tkk + '/' + data.plan_tkk + '</span>',
          '    <span class="flex1 tc"><a href="'+ data.tkk_url +'">前往</a></span>',
          '</p>'
        ].join(""));

        if(data.tkk >= data.plan_tkk) $t_tkk.find('span:last-child').addClass('ready').find('a').html("已完成");

        $dayTask.append($t_tkk);
      }

      // 显示回帖数据
      if(data.plan_reply_posts > 0) {
        var $t_reply_posts = $(['<p class="flexBox">',
          '    <span class="flex1">回帖</span>',
          '    <span class="flex1 tc">' + data.plan_reply_posts + '次</span>',
          '    <span class="flex1 tc">' + data.reply_posts + '/' + data.plan_reply_posts + '</span>',
          '    <span class="flex1 tc"><a href="'+ data.reply_posts_url +'">前往</a></span>',
          '</p>'
        ].join(""));

        if(data.reply_posts >= data.plan_reply_posts) $t_reply_posts.find('span:last-child').addClass('ready').find('a').html("已完成");

        $dayTask.append($t_reply_posts);
      }

      // 显示糖控控评论数据
      if(data.plan_tkk_comments > 0) {
        var $t_tkk_comments = $(['<p class="flexBox">',
          '    <span class="flex1">糖控控评论</span>',
          '    <span class="flex1 tc">' + data.plan_tkk_comments + '次</span>',
          '    <span class="flex1 tc">' + data.tkk_comments + '/' + data.plan_tkk_comments + '</span>',
          '    <span class="flex1 tc"><a href="'+ data.tkk_comments_url +'">前往</a></span>',
          '</p>'
        ].join(""));

        if(data.tkk_comments >= data.plan_tkk_comments) $t_tkk_comments.find('span:last-child').addClass('ready').find('a').html("已完成");

        $dayTask.append($t_tkk_comments);
      }

      $(".vip-task-info").html("提示：完成每日任务，当前会员剩余天数+1");


    });

  }
  
  return init;

});