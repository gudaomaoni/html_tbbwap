/* 
 * 血糖详情查看JS
 */

define(['base'], function(){
	
	/* 页面滚动 */
	var init = function(){
	
		var pageScroll = require('/widget/moudle/pageScroll');
		var vmScroll = new pageScroll("viewMoodCont",{bounce: true});
		
		// 点击图片查看大图
		var PhotoSwipe = require('photoswipe'),
			photoswipeUI = require('photoswipeUI'),
			pswpElement = $('body').find('.pswp').get(0);
		
		$("#viewMoodPhoto>img").on('click', function(){

			// define options (if needed)
		    var options = {
				
				shareEl: false,     // 隐藏分享按钮
				counterEl: false,   // 隐藏计数按钮
		        history: false,     // 不加入历史记录 
		        fullscreenEl: false // 不显示全屏按钮
		        
		    };
		    
		    var items = [
		        {
		            src: this.src,
		            w: parseInt( $(this).attr('_width') ),
		            h: parseInt( $(this).attr('_height') )
		        }
		    ];
		    
		    var gallery = new PhotoSwipe( pswpElement, photoswipeUI, items, options);
			
		    gallery.init();
			
		})
		.on('load', function(){
			var img = new Image();
			img.src = this.src;
			$(this).attr('_width', img.width).attr('_height', img.height);
			
			vmScroll.refresh();
		})
		.attr('src', $("#viewMoodPhoto>img").attr('_src'));
		
	}
	
	return init;
	
});