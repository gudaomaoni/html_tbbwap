define(['/widget/moudle/loadMore','/widget/utils/dateUtil','base'], function(exports){
	
	var init = function(){

		var output = new exports.output;
		
		output.makeItem = function(data){
	
			var MI = $('<section class="moodList-area flexBox"></section>');
			
			var d = data[0].split('-');
			
			var MI_date = $('<div class="moodList-date"><span>'+ d[1] +'<b>'+ DateUtil.getMonthString(d[0]) +'</b></span></div>').appendTo(MI);
			
			var MI_cont = $('<div class="moodList-cont flex1"></div>');
			
			for(var i=0; i<=data[1].length-1; i++){
				
				var dataItem = data[1][i];
				var MI_item = $(['<div class="moodList-item flexBox">',
								'  <div class="ui-moodBV">',
								'    <span class="ui-moodBV-value">'+ dataItem.bloodValue +'</span>',
								'    <span class="ui-moodBV-name">'+ CONFIG.getBdName(dataItem.bloodName) +'</span>',
								'  </div>',
								'  <div class="moodList-itemCont flex1">',
								'    <img src="'+ dataItem.photo.url +'" alt="" />',
								'    <p>',
								'      '+ dataItem.moodText +'',
								'    </p>',
								'  </div>',
								'  <a href="'+ dataItem.moodURL +'" tab="page_viewMood" class="ui-moodUserAreaClick gettab"></a>',
								'</div>'].join(""));
								
				// 判断更改血糖颜色
				var bdArr = CONFIG.getBdRange(dataItem.bloodName),
					$moodBV = MI_item.find('.ui-moodBV');
					
				if( dataItem.bloodValue < bdArr[1] ) $moodBV.addClass('ui-moodBV3');
				else if( dataItem.bloodValue > bdArr[2] ) $moodBV.addClass('ui-moodBV2');
				
				// 判断隐藏图片
				if( dataItem.photo.url == "" || dataItem.photo.url == null ) MI_item.find('.moodList-itemCont img').remove();
				
				MI_cont.append( MI_item );
			}
			
			MI_cont.appendTo(MI)
			
			return MI
		}
		
		output.init("viewMoodList", "moodListData", viewMoodListDataUrl);
	
	}
	
	return init
	
});