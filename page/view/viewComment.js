define(['base'], function(){
	
	// 参数定义
	var $comment, $commentCopy;
	
	
	/*
	 * @ reload加载部分（局部事件绑定，数据加载）
	 */
	
	var init = function(){
		
		// 提示区域
		var vtip,
			vran = Math.random() * 10;
			
		vtip = (vran > 5) ? 1 : 0;

		$(".viewComeTip p").eq(vtip).show();
			
		var loadMore = require('/widget/moudle/loadMore');
		
		var output = new loadMore.output;
		
		output.makeItem = function(data){
	
			var t = ['<section class="ucNewsItem flexBox" data-id="'+ data.id +'">',
					'  <a href="'+ data.ucenterURL +'" class="ucNewsItem-avatar ui-moodAvatar gettab" tab="space">',
					'    <img src="'+ data.avatar +'" alt="">',
					'  </a>',
					'  <div class="ucNewsItem-cont flex1">',
					'    <div class="ucNewsItem-title flexBox">',
					'      <div class="flex1 ucNewsItem-name">',
					'        <span>'+ data.name +'</span>',
					'      </div>',
					'      <b class="viewCome-date">'+ DateUtil.difftimeToStr(false, data.date) +'</b>',
					'    </div>',
					'    <p class="viewCome-words">'+ data.text +'</p>',
					'    <a href="javascript:void(0)" class="ucNewsItem-clickAll" data-id="'+ data.id +'" data-userid="'+ data.userId +'" data-name="'+ data.name +'">&nbsp;</a>',
					'  </div>',
					'</section>'].join("");
			
			var MI = $(t);
			
			if( data.target != "" && data.target != null ) MI.find('.ucNewsItem-name').append(' 回复了 <span>'+ data.target +'</span>');
			
			return MI
		}
		
		output.init("viewComeCont", "viewComeData", viewCommentDataUrl);
		
		
		/*** 表情处理 ****/
		
		var faceModule = require('/widget/face/face'),
			viewFace   = new faceModule();
		
		viewFace.init({
			tid: 'commentCont',
			wid: 'viewComeFace',
			cid: 'viewComeFaceBtn',
			initShow: 'hide',
			effect: 'slide'
		});
		
		$("#viewComeData").on('touchstart', function(){
			
			viewFace.back()
		});

		
		/*** 页面交互 ****/
		
		$comment = $("#commentCont");
		$commentCopy = $("#commentContCopy");
		
		// 扩展点击区域
		$(".viewComeSay-in").on('click', function(){
			
			$comment.trigger('focus')
		});
		
		// 丢失焦点的时候，如果内容为空，还原placeholder和userid
		$comment.on('blur', function(){
			
			if( this.value == "" ){
				
				$(this).attr("placeholder", "发表评论").data('id', '');
			}
			
		});
		
		// 自适应内容高度
		$comment.on('input', function(){
			
			$commentCopy.val( this.value );
			this.style.height = $commentCopy.get(0).scrollHeight + 'px';
			console.log($commentCopy.get(0).scrollHeight + 'px')
		})
		
		// 提交评论内容
		$("#viewComeSubmit").on('click', function(){
			
			if( $comment.val() == "" ){
				
				layer.open({
				    content: '请先填写评论内容！',
				    style: 'background-color:#09C1FF; color:#fff; border:none;',
				    time: 2
				});
				
				return;
			}
			
			// 如果正在发布
			if( $(this).data('posting') == true ){
				layer.open({
				    content: '正在努力发布中...',
				    className: 'layerInfo',
				    time: 2
				});
				return false;
			}
			
			// 标记正在发布
			$(this).data('posting', true);
			
			var posData = {
				
				moodId: viewCommentMoodId,
				text : $comment.val(),
				parentId : $comment.data('id')
			}
			
			console.log( $.param(posData));
			
			$.post(viewCommentPostUrl, $.param(posData), function(response){
				
				$("#viewComeSubmit").data('posting', false);
				
				var data = eval("(" + response + ")");
	
				if( data.success == true ){
			
					layer.open({
					    content: '评论发表成功 √',
					    style: 'background-color:#22AC38; color:#fff; border:none;',
					    time: 2
					});
					
					// 添加最新发布的评论数据到最顶端
					applyCommentData(data.data);
					
					// 改变糖控控评论状态
					changeCommentData(viewCommentMoodId);
					
					// 页面滚动到最顶端
					output.scroller.scrollTo(0, 0);
					
					// 还原评论框状态
					$comment.val("").attr("placeholder", "发表评论").data('id', '').css("height","20px");
					$commentCopy.val("");
					
					// 还原表情状态
					viewFace.back();
					
				}else{
					
					alert('出错啦！请与工作人员进行联系~');
	  			}
						
			})
			
			
		});
		
		// 根据viewCommentParentId，应用评论对象
		if(viewCommentParent[0]!="" && viewCommentParent[1]!=""){
			$comment.val('')
				.attr("placeholder", "回复 " + viewCommentParent[1])
				.data('id', viewCommentParent[0]);
		}
	
	}
	
	
	
	/*
	 * @ 全局事件绑定
	 */
	
	// 点击评论进行回复
	$(document).on('click', '.ucNewsItem-clickAll', function(){
		
		var id = $(this).data('id'),
			userid = $(this).data('userid');
		
		// 如果点击的是同一条记录，不进行操作
		if( $comment.data('id') == id ) return;
		
		// 如果点击的是自己的评论
		if( userid == CommentUserId ){
			
			FUNC.showBotPop('actionCome', id, $(this).parents('.ucNewsItem'));
			return false;
		}
		
		$comment.val('')
				.attr("placeholder", "回复 " + $(this).data('name'))
				.data('id', id)
				.trigger('focus');
				
	});
	
	
	// 将成功发表的评论添加到页面最上方
	function applyCommentData(data){
		
		console.log(data);
		
		var t = ['<section class="ucNewsItem flexBox" data-id="'+ data.id +'">',
				'  <a href="'+ data.ucenterURL +'" data-ajax="true" data-reload="true" class="ucNewsItem-avatar ui-moodAvatar">',
				'    <img src="'+ data.avatar +'" alt="">',
				'  </a>',
				'  <div class="ucNewsItem-cont flex1">',
				'    <div class="ucNewsItem-title flexBox">',
				'      <div class="flex1 ucNewsItem-name">',
				'        <span>'+ data.name +'</span>',
				'      </div>',
				'      <b class="viewCome-date">'+ data.date +'</b>',
				'    </div>',
				'    <p class="viewCome-words">'+ data.text +'</p>',
				'    <a href="javascript:void(0)" class="ucNewsItem-clickAll" data-id="'+ data.id +'" data-userid="'+ data.userId +'" data-name="'+ data.name +'">&nbsp;</a>',
				'  </div>',
				'</section>'].join("");
		
		var MI = $(t);
		
		if( data.target != "" && data.target != null ) MI.find('.ucNewsItem-name').append(' 回复了 <span>'+ data.target +'</span>');
		
		$("#viewComeData").prepend( MI );
	}
	
	// 将页面上对应的糖控控记录上显示的评论状态和数量改变
	function changeCommentData(id){
		
		// 如果是APP访问
		if( tbb.from == 'app' ) return true;
		
		var cs = $('.ui-moodComment-'+id);
		
		cs.find('.comment').addClass('on');
		cs.find('span').each(function(){
			var count = parseInt($(this).html());
			$(this).html( count+1 );
		});
	}
	
	
	return init
	
});