/*
 * 血糖数据页面JS
 */

define(['/widget/moudle/pageScroll', 'base', 'swiper', 'chart'], function(pageScroll) {

  /* 页面滚动 */
  var init = function() {

    var viewBloodPS = new pageScroll("viewBloodPageCont");

    var viewBloodPage = $("#viewBloodNav li");

    // 统计区域数据初始化，第一次加载使用
    var viewBloodStatInit = false;

    // 曲线图数据初始化，第一次加载使用
    var viewBloodGraphInit = false;

    // 调用Swiper插件
    var viewBloodSwiper = new Swiper('#viewBloodSwiper', {
      initialSlide: 0,
      onlyExternal: true,
      iOSEdgeSwipeDetection: true,
      pagination: '.viewBloodNav',
      paginationType: 'custom',
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {

        // 更换分页当前样式
        viewBloodPage.removeClass('on').eq(current - 1).addClass('on');
      },
      speed: 400,
      spaceBetween: 10,
      autoHeight: true,
      onSlideChangeEnd: function(swiper) {
        // Refresh Height
        viewBloodPS.refresh();

        if(!viewBloodStatInit && swiper.activeIndex == 2) {

          // 初始化加载类型：全部数据
          loadStatData(0);
          viewBloodStatInit = true;
          console.log('init stat data')
        }

        if(!viewBloodGraphInit && swiper.activeIndex == 1) loadGraph();
      }
    });

    // 点击进行分页切换
    viewBloodPage.on('click', function() {

      var CurrentPos = $(this).index() > 0 ? $(this).index() : 0;
      viewBloodSwiper.slideTo(CurrentPos);

    });

    // 分月份查看血糖数据
    var viewBloodTable = new Swiper('#viewBloodTable', {
      effect: 'coverflow',
      onlyExternal: true,
      iOSEdgeSwipeDetection: true,
      paginationType: 'custom',
      /* 自定义模式 */
      paginationCustomRender: function(swiper, current, total) {

        // 更换分页当前样式    
      },
      speed: 400,
      spaceBetween: 10,
      autoHeight: true,
      onSlideChangeEnd: function(swiper) {
        // Refresh Height
        viewBloodSwiper.update();
        viewBloodPS.refresh();
      }
    });

    // 加载日期函数
    require('/widget/utils/dateUtil');

    var $monthElem = $(".viewBloodDate").children(),
      $monthPrev = $monthElem.eq(0),
      $monthNext = $monthElem.eq(2),
      $monthText = $monthElem.eq(1);

    var nowDate = new Date(),
      // 当前年月，既为有数据的最后1个月份
      lastDateArr = [DateUtil.datePart('y', nowDate), DateUtil.datePart('M', nowDate)],
      // 展示的数据所在年月
      showDateArr = null,
      // 已经加载过数据的年月集合
      loadDateArr = [],
      loadDateING = false,
      showDateKey = -1;

    // 加载当前年月的数据
    loadMonthData(lastDateArr, viewBloodTable);

    $monthPrev.on('click', function() {

      var monthPrevArr = DateUtil.MonthPrevYM(showDateArr);

      // 如果此月份数据未加载，则进行加载和插入
      if(showDateKey + 1 == loadDateArr.length) {

        loadMonthData(monthPrevArr, viewBloodTable);

        // 如果此月份数据已载入
      } else {

        viewBloodTable.slidePrev();

        setTimeout(function() {
          viewBloodSwiper.update();
        }, 300);

        // 将指针向前移动1
        showDateKey = showDateKey + 1;
        // 显示数据年月更改
        showDateArr = monthPrevArr;
        // 替换页面提示文字
        $monthText.html(loadDateArr[showDateKey]);

        console.log(loadDateArr)
      }

    });

    $monthNext.on('click', function() {

      // 如果是当前时间所在的年月
      if(showDateKey > 0) {

        viewBloodTable.slideNext();

        setTimeout(function() {
          viewBloodSwiper.update();
        }, 300);

        // 将指针向后移动1
        showDateKey = showDateKey - 1;
        // 显示数据年月更改
        showDateArr = DateUtil.MonthNextYM(showDateArr);
        // 替换页面提示文字
        $monthText.html(loadDateArr[showDateKey]);

      } else {

        layer.open({
          content: '没有更新的数据了！',
          className: 'layerInfo',
          time: 3
        });

      }

      console.log(loadDateArr)

    });

    /*
     * 按照月份加载数据
     * 参数 [year, month]
     * 类型 [int, int]
     */

    function loadMonthData(arr, swiper) {

      console.log('load: ' + arr);

      // 如果正在加载中
      if(loadDateING) return;

      layer.open({
        className: 'layerLoading',
        content: '数据加载中...',
        shade: false
      });

      var $Slide = $(['<div class="swiper-slide">',
        '<table>',
        '    <thead>',
        '        <tr>',
        '        	<th rowspan="2">日期</th>',
        '        	<th rowspan="2">凌晨</th>',
        '            <th colspan="2">早餐</th>',
        '            <th colspan="2">午餐</th>',
        '            <th colspan="2">晚餐</th>',
        '            <th rowspan="2">睡前</th>',
        '        </tr>',
        '        <tr>',
        '            <th>空腹</th>',
        '            <th>后</th>',
        '            <th>前</th>',
        '            <th>后</th>',
        '            <th>前</th>',
        '            <th>后</th>',
        '	    </tr>',
        '    </thead>',
        '</table>',
        '</div>'
      ].join(""));

      var $T = $(['<div class="viewBloodVaTab">',
        '	<table>',
        '	    <thead>',
        '        </thead>',
        '    </table>',
        '</div>',
      ].join(""));

      var $thead = $T.find('thead');

      // 标识正在加载数据中
      loadDateING = true;

      // 根据年月获取数据
      $.getJSON(viewBloodValDataUrl, {
        year: arr[0],
        month: arr[1]
      }, function(data) {

        // 如果数据不为空
        if(data != "" && data != null) {

          for(var i = 0; i <= data.length - 1; i++) {

            var $th = $('<tr><td>' + data[i].day + '</td></tr>');

            var blood = data[i].blood;

            for(var k = 0; k <= blood.length - 1; k++) {

              var $td = $('<td tid="' + blood[k].id + '" ttime="' + blood[k].measure_date + '" tpoint="' + blood[k].measure_point + '">' + (blood[k].value == 0 ? "" : blood[k].value) + '</td>');

              // 判断更改血糖颜色
              if(blood[k].value != 0 && blood[k].value != null) {

                var bdArr = CONFIG.getBdRange(k + 1);

                if(blood[k].value < bdArr[1]) $td.addClass('red value');
                else if(blood[k].value > bdArr[2]) $td.addClass('yellow value');
                else $td.addClass('green value');
              }

              $th.append($td);
            }

            $thead.append($th);

          }

          $Slide.append($T);
        }

        swiper.prependSlide('<div class="swiper-slide">' + $Slide.html() + '</div>');

        var yms = DateUtil.YMtoString(arr);

        // 关闭加载提示
        layer.closeAll();
        // 存入已加载数据
        loadDateArr.push(yms);

        // 向前slide
        swiper.slidePrev();

        viewBloodSwiper.update();

        viewBloodPS.refresh();

        // 将指针向前移动1
        showDateKey = showDateKey + 1;
        // 显示数据年月更改
        showDateArr = arr;
        // 替换页面提示文字
        $monthText.html(yms);

        loadDateING = false;

        console.log(loadDateArr)

      });

    }

    /* 点击编辑血糖 */
    $(document).on('click', '.viewBloodVaTab .value', function() {

      // 如果不是本人查看，无法编辑
      if(viewBloodDataUid != tbb.uid) return false;

      var id = parseInt($(this).attr("tid")),
        time = $(this).attr("ttime"),
        point = parseInt($(this).attr("tpoint")),
        value = parseFloat($(this).text()),
        _this = this;

      var T = ['<ul class="viewBdEdit">',
        '<li><span>测量时间</span><input type="text" value="' + DateUtil.dateToStr(DateUtil.longToDate(time * 1000), 'yyyy年MM月dd日') + '" readonly="true" /></li>',
        '<li><span>测量点</span><input type="text" value="' + CONFIG.getBdName(point) + '" readonly="true" /></li>',
        '<li><span>血糖值</span><input type="text" value="' + value + '" class="number" id="viewBdEdit_value" /></li>',
        '</ul>',
        '<button class="viewBdEdit-del" id="viewBdEdit_delete"> <span></span>删除此条数据</button>'
      ].join("");

      layer.open({
        type: 1,
        style: 'width:80%; max-width:640px; border-radius:5px',
        title: [
          '编辑血糖数据',
          'border-bottom: 1px solid #f1f1f1'
        ],
        //anim: 'up',
        content: T,
        btn: ['保存', '取消'],
        shadeClose: false,
        success: function(elem) {

          // 输入框聚焦到最后
          var inputE = $(elem).find('.number').get(0);
          inputE.focus();
          inputE.selectionStart = inputE.selectionEnd = inputE.value.length;

          // 删除本条数据
          $(elem).find('#viewBdEdit_delete').on('click', function() {
            // 询问框，因为使用layer会让上一层销毁，所以使用系统原生代码
            if(confirm("您确认要删除此条数据吗？")) {
              // 删除数据
              layer.open({
                type: 2,
                content: '正在删除中'
              });

              $.post(CONFIG.deleteURL, {
                action: 'delete',
                id: id,
                hash: tbb.formhash
              }, function(response) {

                var data = eval("(" + response + ")");

                layer.closeAll();

                if(data.success == true) {

                  layer.open({
                    content: '已成功删除 √',
                    btn: ['好的']
                  });

                  // 移除HTML
                  $(_this).removeClass().empty();

                } else {

                  alert('出错啦！请与工作人员进行联系~');
                }

              });
            }
          });
        },

        yes: function(index) {

          var newVal = parseFloat($("#viewBdEdit_value").val());

          // 如果血糖值有变化
          if(newVal != value) {

            if(newVal < 1.1 || newVal > 33.3) {
              alert('请输入1.1~33.3之间的数字！');
              $("#viewBdEdit_value").focus();
              return false;
            }

            $.post(CONFIG.updateURL + '/' + id, {
              uid: viewBloodDataUid,
              hash: tbb.formhash,
              value: newVal
            }, function(response) {

              if( typeof(response) != 'object' ) var data = eval("(" + response + ")");
              else var data = response;

              layer.closeAll();

              if(data.success == true) {

                layer.open({
                  content: '已保存修改 √',
                  className: 'layerSuccess',
                  time: 2
                });

                // 修改HTML
                var bdArr = CONFIG.getBdRange(point);

                if(newVal < bdArr[1]) $(_this).attr("class", 'red value').html(newVal);
                else if(newVal > bdArr[2]) $(_this).attr("class", 'yellow value').html(newVal);
                else $(_this).attr("class", 'green value').html(newVal);

              } else {

                alert('出错啦！请与工作人员进行联系~');
              }

            });

          } else layer.close(index);

        }

      });

    });

    /**** 曲线图区域 ****/

    function loadGraph() {

      layer.open({
        className: 'layerLoading',
        content: '数据加载中...',
        shade: false
      });

      // 初始化加载类型：全部数据
      var GP = require('/widget/d3/graph');
      var graph = new GP();
      var btn = $("#viewBdGraphBtn span");

      $.getJSON(viewBloodGraphUrl, function(json) {

        graph.init({}, json);

        btn.on('click', function() {

          btn.removeClass('on');
          $(this).addClass('on');

          var level = parseInt($(this).attr('level'));
          graph.zoomToLevel(level);

        });

        viewBloodGraphInit = true;

        // 关闭加载提示
        layer.closeAll();

        console.log('init graph data')
      });
    }

    /**** 统计区域 ****/

    // JSON数据缓存集合
    var statDataSet = {};

    // 统计数据chart圆盘对象集合（最近一周、最近一个月、最近三个月）
    var viewCharts = [null, null, null];

    $("#viewBloodState").on('change', function() {

      var typeId = this.value;

      if(typeof(statDataSet[typeId]) == 'undefined') loadStatData(typeId);
      else applyStatData(statDataSet[typeId]);

    });

    // 根据选择的数据类型，加载统计数据
    function loadStatData(typeId) {

      $.getJSON(viewBloodStatDataUrl, {
        id: parseInt(typeId)
      }, function(data) {

        // 数据渲染
        applyStatData(data);

        // 存入缓存集合
        statDataSet[typeId] = data;

        console.log(statDataSet);
      });
    }

    // 获取需要修改的对象
    var statTimeTotal = $(".viewBdStat-chartText span"),
      statTimeNormal = $(".viewBdStat-dataNormal"),
      statTimeLow = $(".viewBdStat-dataLow"),
      statTimeHigh = $(".viewBdStat-dataHigh"),
      bloodMean = $(".viewBdStat-mean"),
      bloodLow = $(".viewBdStat-low"),
      bloodHigh = $(".viewBdStat-high");

    // 根据数据进行页面渲染
    function applyStatData(data) {

      for(var i = 0; i <= data.length - 1; i++) {

        var pieData = [{
          value: data[i].normal,
          color: "#22AC38",
          label: "正常"
        }, {
          value: data[i].low,
          color: "#FF0000",
          label: "偏低"
        }, {
          value: data[i].high,
          color: "orange",
          label: "偏高"
        }];

        // 如果chart已经存在了，清空它
        if(viewCharts[i] != null) viewCharts[i].destroy();

        var ctx = document.getElementById("chart-area" + (i + 1)).getContext("2d");
        viewCharts[i] = new Chart(ctx).Doughnut(pieData, {
          segmentStrokeWidth: 0,
          percentageInnerCutout: 84
        });

        // 总测量次数
        statTimeTotal.eq(i).html(data[i].total);

        var percent_normal = data[i].total == 0 ? 0 : (data[i].normal / data[i].total * 100).toFixed(1),
          percent_low = data[i].total == 0 ? 0 : (data[i].low / data[i].total * 100).toFixed(1),
          percent_high = data[i].total == 0 ? 0 : (data[i].high / data[i].total * 100).toFixed(1);

        // 正常、偏低、偏高
        statTimeNormal.eq(i).html('<b>正常 ' + percent_normal + '%</b> ' + data[i].normal + '次');
        statTimeLow.eq(i).html('<b>偏低 ' + percent_low + '%</b> ' + data[i].low + '次');
        statTimeHigh.eq(i).html('<b>偏高 ' + percent_high + '%</b> ' + data[i].high + '次');

        // 平均值、最低值、最高值
        bloodMean.eq(i).html(data[i].range[0]);
        bloodLow.eq(i).html(data[i].range[1]);
        bloodHigh.eq(i).html(data[i].range[2]);
      }
    }

  }

  return init

});