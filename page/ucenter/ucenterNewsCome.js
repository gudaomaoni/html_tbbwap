define(['/widget/moudle/loadMore', 'base'], function(exports, cssPreload){

	var init = function(){

		var output = new exports.output;
		
		output.makeItem = function(data){
	
			var t = ['<section class="ucNewsItem flexBox" data-id="'+ data.id +'">',
			'	<a href="'+ data.ucenterURL +'" class="ui-moodAvatar ucNewsItem-avatar gettab" tab="space">',
			'		<img src="'+ data.avatar +'" alt="">',
			'	</a>',
			'	<div class="ucNewsItem-cont flex1">',
			'		<div class="ucNewsItem-title flexBox">',
			'			<div class="flex1">',
			'				<p><a href="">'+ data.name +'</a>  在 说说：'+ data.moodText +' 中回复了你</p>',
			'			</div>',
			'			<div class="ui-moodBV">',
			'				<span class="ui-moodBV-value">'+ data.bloodValue +'</span>',
			'				<span class="ui-moodBV-name">'+ CONFIG.getBdName(data.bloodName) +'</span>',
			'			</div>',
			'		</div>',
			'		<div class="ucNewsComeWords">',
			'			<p>'+ data.text +'</p>',
			'		</div>',
			'		<div class="ucNewsItem-date">'+ DateUtil.difftimeToStr(false, data.date) +'</div>',
			'		<a href="'+ data.commentURL +'" tab="page_viewComment" class="ucNewsItem-clickAll gettab">&nbsp;</a>',
			'		<a href="'+ data.moodURL +'" tab="page_viewMood" class="ucNewsItem-clickSource gettab">&nbsp;</a>',
			'	</div>',
			'</section>'].join("");
			
			var MI = $(t);
			
			// 判断更改血糖颜色
			var bdArr = CONFIG.getBdRange(data.bloodName),
				$moodBV = MI.find('.ui-moodBV');
				
			if( data.bloodValue < bdArr[1] ) $moodBV.addClass('ui-moodBV3');
			else if( data.bloodValue > bdArr[2] ) $moodBV.addClass('ui-moodBV2');
			
			return MI
		}
		
		output.init("ucNewsComeCont", "ucNewsComeData", ucNewsComeDataUrl);

	}
	
	return init
	
});