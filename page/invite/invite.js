define(['zepto'], function() {

  // fastclick点击
  var FastClick = require('fastclick');
  FastClick.attach($("#body_invite").get(0));

  // 判断修改页面内容高度
  if( !($(".invite-btn").length > 0) ){
    $('.body_main').css("height","100%");
  }

  // 邀请好友加入
  $(".invite-btn").on('click', function() {
    $(".sac-share-bg").show();
    $(".sac-share").show().one('click', function() {
      $(".sac-share-bg").hide();
      $(this).hide();
    });
  });

});